# README #

Data reduction and a hierarchical approach for the structure alignment of proteins is
used by applying an algorithm, which is called Relative Orientation of Secondary
Structure elements (ROSS).


This project is protected under GNU Lesser General Public License, version 2.1.
GNU LESSER GENERAL PUBLIC LICENSE

Version 2.1, February 1999

Copyright (C) 1991, 1999 Free Software Foundation, Inc.
51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
Everyone is permitted to copy and distribute verbatim copies
of this license document, but changing it is not allowed.

[This is the first released version of the Lesser GPL.  It also counts
 as the successor of the GNU Library Public License, version 2, hence
 the version number 2.1.]

see  https://www.gnu.org/licenses/lgpl-2.1.html


* Abdallah Mohammed Abbas
* Ahmed Ali Ali
* Mohammed  Ali Elshiemy
* Mohammed  Abbas 
* Shukri Saleh Awadh