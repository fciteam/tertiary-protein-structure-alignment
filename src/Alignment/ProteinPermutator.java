/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignment;

import Model.Coordinate;
import Model.Matrix;
import Model.Protein;
import java.util.ArrayList;
import org.jblas.DoubleMatrix;

/**
 * @author ahmed
 */
public class ProteinPermutator
{

    Protein newProtein;
    Protein protein;
    ArrayList<Protein> proteins;

    public ProteinPermutator(Protein protein1)
    {
        this.protein = protein1;
        try
        {
            newProtein = (Protein) protein.clone();
        }
        catch (CloneNotSupportedException ex)
        {
            //System.out.println("Not clonebale!");
        }
        proteins = new ArrayList<>();
    }

    public Protein getNewProtein()
    {
        return newProtein;
    }

    public void setNewProtein(Protein newProtein)
    {
        this.newProtein = newProtein;
    }

    public Protein getProtein()
    {
        return protein;
    }

    public void setProtein(Protein protein)
    {
        this.protein = protein;
    }

    public ArrayList<Protein> permuteProtein() throws CloneNotSupportedException
    {
        for (int i = 0; i < 24; i++)
        {
            for (int j = 0; j < protein.getListOfChains().size(); j++)
            {
                if (!protein.getListOfChains().get(j).getSseVector().isEmpty())
                {
                    for (int k = 0;
                         k < protein.getListOfChains().get(j).getSseVector().
                            size();
                         k++)
                    {
                        newProtein.getListOfChains().get(j).getSseVector().
                                get(k)
                                .setCenteroid(rotatedPoints(
                                                (Coordinate) protein.
                                                getListOfChains()
                                                .get(j)
                                                .getSseVector()
                                                .get(k)
                                                .getCenteroid().clone(),
                                                i));
                        newProtein.getListOfChains().get(j).getSseVector().
                                get(k)
                                .setResultant(rotatedPoints(
                                                (Coordinate) protein.
                                                getListOfChains()
                                                .get(j)
                                                .getSseVector()
                                                .get(k)
                                                .getResultant().clone(),
                                                i));
                    }

                    newProtein.getListOfChains().get(j).getDescriptor().
                            getInterdirectionsPhi().clear();
                    newProtein.getListOfChains().get(j).getDescriptor().
                            getInterdirectionsSymbols().clear();
                    newProtein.getListOfChains().get(j).getDescriptor().
                            getInterdirectionsTheta().clear();

                    newProtein.getListOfChains().get(j).getDescriptor().
                            getProximity().clear();
                    newProtein.getListOfChains().get(j).getDescriptor().
                            getResultantsPhi().clear();
                    newProtein.getListOfChains().get(j).getDescriptor().
                            getResultantsSymbols().clear();
                    newProtein.getListOfChains().get(j).getDescriptor().
                            getResultantsTheta().clear();
                    newProtein.getListOfChains().get(j).getDescriptor().
                            setTypes("");
                    newProtein.processSSE(j);
                }
            }

            proteins.add(newProtein);
            newProtein = (Protein) protein.clone();
        }

        return proteins;
    }

    public Coordinate rotatedPoints(Coordinate XYZ, int rotateNum)
    {
        rotateNum += 1;
        double[][] res =
        {
            {
                XYZ.getX()
            },
            {
                XYZ.getY()
            },
            {
                XYZ.getZ()
            }
        };
        DoubleMatrix resM = new DoubleMatrix(res);

        if (rotateNum == 1)
        {
            double[][] d =
            {
                {
                    1, 0, 0
                },
                {
                    0, 1, 0
                },
                {
                    0, 0, 1
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 2)
        {
            double[][] d =
            {
                {
                    0, 0, 1
                },
                {
                    0, 1, 0
                },
                {
                    -1, 0, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 3)
        {
            double[][] d =
            {
                {
                    -1, 0, 0
                },
                {
                    0, 1, 0
                },
                {
                    0, 0, -1
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 4)
        {
            double[][] d =
            {
                {
                    0, 0, -1
                },
                {
                    0, 1, 0
                },
                {
                    1, 0, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 5)
        {
            double[][] d =
            {
                {
                    0, -1, 0
                },
                {
                    1, 0, 0
                },
                {
                    0, 0, 1
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 6)
        {
            double[][] d =
            {
                {
                    0, 0, 1
                },
                {
                    1, 0, 0
                },
                {
                    0, 1, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 7)
        {
            double[][] d =
            {
                {
                    0, 1, 0
                },
                {
                    1, 0, 0
                },
                {
                    0, 0, -1
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 8)
        {
            double[][] d =
            {
                {
                    0, 0, -1
                },
                {
                    1, 0, 0
                },
                {
                    0, -1, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 9)
        {
            double[][] d =
            {
                {
                    0, 1, 0
                },
                {
                    -1, 0, 0
                },
                {
                    0, 0, 1
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 10)
        {
            double[][] d =
            {
                {
                    0, 0, 1
                },
                {
                    -1, 0, 0
                },
                {
                    0, -1, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 11)
        {
            double[][] d =
            {
                {
                    0, -1, 0
                },
                {
                    -1, 0, 0
                },
                {
                    0, 0, -1
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 12)
        {
            double[][] d =
            {
                {
                    0, 0, -1
                },
                {
                    -1, 0, 0
                },
                {
                    0, 1, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 13)
        {
            double[][] d =
            {
                {
                    1, 0, 0
                },
                {
                    0, 0, -1
                },
                {
                    0, 1, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 14)
        {
            double[][] d =
            {
                {
                    0, 1, 0
                },
                {
                    0, 0, -1
                },
                {
                    -1, 0, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 15)
        {
            double[][] d =
            {
                {
                    -1, 0, 0
                },
                {
                    0, 0, -1
                },
                {
                    0, -1, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 16)
        {
            double[][] d =
            {
                {
                    0, -1, 0
                },
                {
                    0, 0, -1
                },
                {
                    1, 0, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 17)
        {
            double[][] d =
            {
                {
                    1, 0, 0
                },
                {
                    0, -1, 0
                },
                {
                    0, 0, -1
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 18)
        {
            double[][] d =
            {
                {
                    0, 0, -1
                },
                {
                    0, -1, 0
                },
                {
                    -1, 0, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 19)
        {
            double[][] d =
            {
                {
                    -1, 0, 0
                },
                {
                    0, -1, 0
                },
                {
                    0, 0, 1
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 20)
        {
            double[][] d =
            {
                {
                    0, 0, 1
                },
                {
                    0, -1, 0
                },
                {
                    1, 0, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 21)
        {
            double[][] d =
            {
                {
                    1, 0, 0
                },
                {
                    0, 0, 1
                },
                {
                    0, -1, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 22)
        {
            double[][] d =
            {
                {
                    0, -1, 0
                },
                {
                    0, 0, 1
                },
                {
                    -1, 0, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 23)
        {
            double[][] d =
            {
                {
                    -1, 0, 0
                },
                {
                    0, 0, 1
                },
                {
                    0, 1, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }
        else if (rotateNum == 24)
        {
            double[][] d =
            {
                {
                    0, 1, 0
                },
                {
                    0, 0, 1
                },
                {
                    1, 0, 0
                }
            };
            Matrix D = new Matrix(d);
            resM = D.transpose().mmul(resM);
        }

        res = resM.toArray2();
        XYZ.setX(res[0][0]);
        XYZ.setY(res[1][0]);
        XYZ.setZ(res[2][0]);

        return XYZ;
    }
}
