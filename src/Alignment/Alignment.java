/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignment;

import Model.Protein;

import java.util.ArrayList;


/**
 *
 * @author Abdallah
 */
public class Alignment
{
    private ArrayList<Protein> permuteProtein24;
    private DescriptorComparor descriptorComparor;
    private ProteinPermutator  permutator;
    private final boolean      printing;

    public Alignment()
    {
        printing = false;
    }

    public Alignment(boolean isB)
    {
        printing = isB;
    }

    /**
     *
     * @param queryFile
     * @param targetFile
     * @param c1
     * @param c2
     * @throws java.lang.Exception
     */
    public void align(String queryFile, String targetFile, char c1, char c2)
               throws Exception
    {
        ProteinReader queryReader = new ProteinReader();
        queryReader.ReaderFun(queryFile);

        Protein query = queryReader.getProtein();

        ProteinReader tragetReader = new ProteinReader();
        tragetReader.ReaderFun(targetFile);

        Protein target = tragetReader.getProtein();

        permutator = new ProteinPermutator(query);
        permuteProtein24 = permutator.permuteProtein();

        int index1 = permuteProtein24.get(0).getChainIndex(c1);
        int index2 = target.getChainIndex(c2);

        if (index1 == -1)
        {
            index1 = 0;
        }

        if (index2 == -1)
        {
            index2 = 0;
        }

        descriptorComparor = new DescriptorComparor();
        descriptorComparor.intialAllignment(permuteProtein24, target,
                                            permuteProtein24.get(0)
                                                            .getListOfChains()
                                                            .get(index1)
                                                            .getChainId(),
                                            target.getListOfChains().get(index2)
                                                  .getChainId());

        int[][]            res = descriptorComparor.getMaxScores();
        ArrayList<Integer> calculateSSEMatchs;
        calculateSSEMatchs = descriptorComparor.calculateSSEMatchs(descriptorComparor.getBestRotaion(),
                                                                   target.getListOfChains()
                                                                         .get(0),
                                                                   res, printing);
        descriptorComparor.calculateFinalAlignment(descriptorComparor.getBestRotaion(),
                                                   target.getListOfChains()
                                                         .get(0),
                                                   calculateSSEMatchs,
                                                   descriptorComparor.getBestRotaionNumber(),
                                                   3);
    }

    public DescriptorComparor getDescriptorComparor()
    {
        return descriptorComparor;
    }

    public ProteinPermutator getPermutator()
    {
        return permutator;
    }

    public ArrayList<Protein> getPermuteProtein24()
    {
        return permuteProtein24;
    }

    public void setDescriptorComparor(DescriptorComparor descriptorComparor)
    {
        this.descriptorComparor = descriptorComparor;
    }

    public void setPermutator(ProteinPermutator permutator)
    {
        this.permutator = permutator;
    }

    public void setPermuteProtein24(ArrayList<Protein> permuteProtein24)
    {
        this.permuteProtein24 = permuteProtein24;
    }
}
