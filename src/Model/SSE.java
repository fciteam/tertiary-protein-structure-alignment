/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;


/**
 * @author Abdallah
 */
public class SSE implements Cloneable
{
    private Coordinate centeroid; //x,y,z
    private Coordinate resultant; //x,y,z
    private char       sseType;
    private int        chainIndex; //2
    private int        endIndex; //4
    private int        index; //11
    private int        intialIndex; //3
    private int        sseClass;

    public SSE(int chainIndex, int index, int intialIndex, int endIndex,
               Coordinate centeriod, Coordinate resultant, int sseClass,
               char sseType)
    {
        this.chainIndex = chainIndex;
        this.index = index;
        this.intialIndex = intialIndex;
        this.endIndex = endIndex;
        this.centeroid = centeriod;
        this.resultant = resultant;
        this.sseClass = sseClass;
        this.sseType = sseType;
    }

    public SSE()
    {
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return new SSE(chainIndex, index, intialIndex, endIndex,
                       new Coordinate(centeroid.getX(), centeroid.getY(),
                                      centeroid.getZ()),
                       new Coordinate(resultant.getX(), resultant.getY(),
                                      resultant.getZ()), sseClass, sseType);
    }

    public Coordinate getCenteroid()
    {
        return centeroid;
    }

    public int getChainIndex()
    {
        return chainIndex;
    }

    public int getEndIndex()
    {
        return endIndex;
    }

    public int getIndex()
    {
        return index;
    }

    public int getIntialIndex()
    {
        return intialIndex;
    }

    public Coordinate getResultant()
    {
        return resultant;
    }

    public int getSseClass()
    {
        return sseClass;
    }

    public char getSseType()
    {
        return sseType;
    }

    public void setCenteroid(Coordinate centeriod)
    {
        this.centeroid = centeriod;
    }

    public void setChainIndex(int chainIndex)
    {
        this.chainIndex = chainIndex;
    }

    public void setEndIndex(int endIndex)
    {
        this.endIndex = endIndex;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public void setIntialIndex(int intialIndex)
    {
        this.intialIndex = intialIndex;
    }

    public void setResultant(Coordinate resultant)
    {
        this.resultant = resultant;
    }

    public void setSseClass(int sseClass)
    {
        this.sseClass = sseClass;
    }

    public void setSseType(char sseType)
    {
        this.sseType = sseType;
    }
}
