package Model;

import java.util.ArrayList;

/**
 * @author Abdallah
 */
public class Sheet extends SecondaryStructure implements Cloneable
{

    private int numberOfStrands;
    private int strandSenseWithRespectToPrevious;
    private AtomResidue chainAtom1;
    private AtomResidue chainAtom2;

    /**
     * Default constructor
     */
    public Sheet()
    {
        super();
        sseType = 'S';
        this.chainAtom1 = new AtomResidue();
        this.chainAtom2 = new AtomResidue();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        ArrayList<Atom> atomsTemp = new ArrayList<>();
        for (int i = 0; i < this.atoms.size(); i++)
        {
            atomsTemp.add((Atom) this.atoms.get(i).clone());
        }
        return new Sheet(numberOfStrands, strandSenseWithRespectToPrevious,
                         (AtomResidue) chainAtom1.clone(),
                         (AtomResidue) chainAtom2.clone(), serialNumber,
                         identifier,
                         (Residue) initial.clone(), (Residue) terminal.clone(),
                         atomsTemp,
                         (Coordinate) start.clone(), (Coordinate) end.clone(),
                         (Coordinate) centeriod.clone(), (Coordinate) resultant.
                clone(), sseClass,
                         sseType); //To change body of generated methods, choose Tools | Templates.
    }

    public Sheet(int numberOfStrands, int strandSenseWithRespectToPrevious,
                 AtomResidue chainAtom1, AtomResidue chainAtom2,
                 int serialNumber, String identifier, Residue initial,
                 Residue terminal, ArrayList<Atom> atoms, Coordinate start,
                 Coordinate end, Coordinate centeriod, Coordinate resultant,
                 int sseClass, char type)
    {
        super(serialNumber, identifier, initial, terminal, atoms, start, end,
              centeriod, resultant, sseClass, type);

        this.numberOfStrands = numberOfStrands;
        this.strandSenseWithRespectToPrevious = strandSenseWithRespectToPrevious;
        this.chainAtom1 = chainAtom1;
        this.chainAtom2 = chainAtom2;
    }

    public Sheet(int numberOfStrands, int strandSenseWithRespectToPrevious,
                 AtomResidue chainAtom1, AtomResidue chainAtom2,
                 int serialNumber, String identifier, Residue initial,
                 Residue terminal, ArrayList<Atom> atoms, Coordinate start,
                 Coordinate end, Coordinate centeriod, Coordinate resultant,
                 int sseClass)
    {
        super(serialNumber, identifier, initial, terminal, atoms, start, end,
              centeriod, resultant, sseClass);
        this.numberOfStrands = numberOfStrands;
        this.strandSenseWithRespectToPrevious = strandSenseWithRespectToPrevious;
        this.chainAtom1 = chainAtom1;
        this.chainAtom2 = chainAtom2;
    }

    /**
     * @param numberOfStrands
     * @param strandSenseWithRespectToPrevious
     * @param chainAtom1
     * @param chainAtom2
     */
    public Sheet(int numberOfStrands, int strandSenseWithRespectToPrevious,
                 AtomResidue chainAtom1, AtomResidue chainAtom2)
    {
        super();
        this.numberOfStrands = numberOfStrands;
        this.strandSenseWithRespectToPrevious = strandSenseWithRespectToPrevious;
        this.chainAtom1 = chainAtom1;
        this.chainAtom2 = chainAtom2;
    }

    public Sheet(int numberOfStrands, int strandSenseWithRespectToPrevious,
                 AtomResidue chainAtom1, AtomResidue chainAtom2,
                 int serialNumber, String identifier, Residue initial,
                 Residue terminal, ArrayList<Atom> atoms, Coordinate start,
                 Coordinate end, Coordinate center)
    {
        super(serialNumber, identifier, initial, terminal, atoms, start, end,
              center);
        this.numberOfStrands = numberOfStrands;
        this.strandSenseWithRespectToPrevious = strandSenseWithRespectToPrevious;
        this.chainAtom1 = chainAtom1;
        this.chainAtom2 = chainAtom2;
    }

    /**
     * @return
     */
    public AtomResidue getchainAtom1()
    {
        return chainAtom1;
    }

    /**
     * @param chainAtom1
     */
    public void setchainAtom1(AtomResidue chainAtom1)
    {
        this.chainAtom1 = chainAtom1;
    }

    /**
     * @return
     */
    public AtomResidue getchainAtom2()
    {
        return chainAtom2;
    }

    /**
     * @param chainAtom2
     */
    public void setchainAtom2(AtomResidue chainAtom2)
    {
        this.chainAtom2 = chainAtom2;
    }

    /**
     * @return
     */
    public int getNumberOfStrands()
    {
        return numberOfStrands;
    }

    /**
     * @param numberOfStrands
     */
    public void setNumberOfStrands(int numberOfStrands)
    {
        this.numberOfStrands = numberOfStrands;
    }

    /**
     * @return
     */
    public int getStrandSenseWithRespectToPrevious()
    {
        return strandSenseWithRespectToPrevious;
    }

    /**
     * @param strandSenseWithRespectToPrevious
     */
    public void setStrandSenseWithRespectToPrevious(
            int strandSenseWithRespectToPrevious)
    {
        this.strandSenseWithRespectToPrevious = strandSenseWithRespectToPrevious;
    }

    /**
     * @param string
     */
    public void extract(String string) throws Exception
    {
        setSerialNumber(Integer.parseInt(string.substring(7, 10).trim()));
        setIdentifier(string.substring(11, 14));
        setNumberOfStrands(Integer.parseInt(string.substring(14, 16).trim()));
        setInitial(new Residue(string.substring(17, 20), string.charAt(21),
                               Integer.parseInt(string.substring(22, 26).trim()),
                               string.charAt(26)));
        setTerminal(new Residue(string.substring(28, 31), string.charAt(32),
                                Integer.
                parseInt(string.substring(33, 37).trim()),
                                string.charAt(37)));

        try
        {
            setStrandSenseWithRespectToPrevious(Integer.parseInt(
                    string.substring(38, 40).trim()));
            setchainAtom1(new AtomResidue(string.substring(41, 45),
                                          string.substring(45, 48), string.
                    charAt(49),
                                          Integer.parseInt(string.substring(50,
                                                                            54).
                    trim()),
                                          string.charAt(54)));
            setchainAtom2(new AtomResidue(string.substring(56, 60),
                                          string.substring(60, 63), string.
                    charAt(64),
                                          Integer.parseInt(string.substring(65,
                                                                            69).
                    trim()),
                                          string.charAt(69)));
        }
        catch (NumberFormatException exception)
        {
            //throw new Exception("Missed part of sheet!");
        }
    }
}
