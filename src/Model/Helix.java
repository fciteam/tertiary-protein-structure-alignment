package Model;

import java.util.ArrayList;

/**
 * Helix class holds all Helix property needed to process
 *
 * @author Abdallah
 */
public class Helix extends SecondaryStructure implements Cloneable
{

    private String comment;
    private int lengthOfHelix;

    /**
     * *
     */
    public Helix()
    {
        super();
        this.comment = "";
        this.lengthOfHelix = 0;
        this.sseType = 'H';

    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        ArrayList<Atom> atomsTemp = new ArrayList<>();
        for (Atom atom : this.atoms)
        {
            atomsTemp.add((Atom) atom.clone());
        }
        return new Helix(comment, lengthOfHelix, serialNumber, identifier,
                         (Residue) initial.clone(),
                         (Residue) terminal.clone(), atomsTemp,
                         (Coordinate) start.clone(),
                         (Coordinate) end.clone(), (Coordinate) centeriod.
                clone(),
                         (Coordinate) resultant.clone(), sseClass, sseType);
    }

    public Helix(String comment, int lengthOfHelix, int serialNumber,
                 String identifier, Residue initial, Residue terminal,
                 ArrayList<Atom> atoms, Coordinate start, Coordinate end,
                 Coordinate centeriod, Coordinate resultant, int sseClass,
                 char ssType)
    {
        super(serialNumber, identifier, initial, terminal, atoms, start, end,
              centeriod, resultant, sseClass, ssType);
        this.comment = comment;
        this.lengthOfHelix = lengthOfHelix;

    }

    /**
     * @param serialNumber
     * @param identifier
     * @param initial
     * @param terminal
     * @param typeOfHelix
     * @param comment
     * @param lengthOfHelix
     */
    public Helix(int serialNumber, String identifier, Residue initial,
                 Residue terminal, int typeOfHelix, String comment,
                 int lengthOfHelix)
    {
        super(serialNumber, identifier, initial, terminal);
        this.comment = comment;
        this.lengthOfHelix = lengthOfHelix;
    }

    /**
     *
     * @return
     */
    /**
     * @return
     */
    public String getComment()
    {
        return comment;
    }

    /**
     * @return
     */
    public int getLengthOfHelix()
    {
        return lengthOfHelix;
    }

    /**
     * @param comment
     */
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    /**
     * @param lengthOfHelix
     */
    public void setLengthOfHelix(int lengthOfHelix)
    {
        this.lengthOfHelix = lengthOfHelix;
    }

    /**
     * @param helix
     */
    public void extract(String helix)
    {
        setSerialNumber(Integer.parseInt(helix.substring(7, 10).trim()));
        setIdentifier(helix.substring(11, 14));
        setInitial(new Residue(helix.substring(15, 18), helix.charAt(19),
                               Integer.parseInt(helix.substring(21, 25).trim()),
                               helix.charAt(25)));
        setTerminal(new Residue(helix.substring(27, 30), helix.charAt(31),
                                Integer.parseInt(helix.substring(33, 37).trim()),
                                helix.charAt(39)));
        setSseClass(Integer.parseInt(helix.substring(38, 40).trim()));
        setComment(helix.substring(40, 70));
        setLengthOfHelix(Integer.parseInt(helix.substring(71, 76).trim()));

    }

}
