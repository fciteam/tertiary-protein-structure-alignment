/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 * @author Abdallah
 */
public class Chain implements Cloneable
{

    private Sequence sequence;
    private char chainId;
    private int intialAtom;
    private int endAtom;
    private ArrayList<Integer> residueIndex;
    private ArrayList<Integer> residueCAIndex;
    private ArrayList<Coordinate> residuePointCA;
    private ArrayList<SSE> sseVector;
    private Descriptor descriptor;

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        ArrayList<Integer> residueIndexTemp = new ArrayList<>(this.residueIndex);
        ArrayList<Integer> residueCAIndexTemp = new ArrayList<>(
                this.residueCAIndex);

        ArrayList<Coordinate> residuePointCATemp = new ArrayList<>();
        for (Coordinate aResiduePointCA : this.residuePointCA)
        {
            residuePointCATemp.add((Coordinate) aResiduePointCA.clone());
        }
        ArrayList<SSE> sseVectorTemp = new ArrayList<>();
        for (SSE aSseVector : this.sseVector)
        {
            sseVectorTemp.add((SSE) aSseVector.clone());
        }

        return new Chain(sequence, chainId, intialAtom, endAtom,
                         residueIndexTemp, residueCAIndexTemp,
                         residuePointCATemp, sseVectorTemp,
                         (Descriptor) descriptor.clone()); //To change body of generated methods, choose Tools | Templates.
    }

    public Chain()
    {
        this.sequence = new Sequence();
        this.intialAtom = 0;
        this.endAtom = 0;
        this.residueIndex = new ArrayList<>();
        this.residueCAIndex = new ArrayList<>();
        this.residuePointCA = new ArrayList<>();
        this.sseVector = new ArrayList<>();
        this.descriptor = new Descriptor();
    }

    public Chain(Sequence sequence, char chainId, int intialAtom, int endAtom,
                 ArrayList<Integer> residueIndex,
                 ArrayList<Integer> residueCAIndex,
                 ArrayList<Coordinate> residuePointCA, ArrayList<SSE> sseVector,
                 Descriptor descriptor)
    {
        this.sequence = sequence;
        this.chainId = chainId;
        this.intialAtom = intialAtom;
        this.endAtom = endAtom;
        this.residueIndex = residueIndex;
        this.residueCAIndex = residueCAIndex;
        this.residuePointCA = residuePointCA;
        this.sseVector = sseVector;
        this.descriptor = descriptor;
    }

    public Descriptor getDescriptor()
    {
        return descriptor;
    }

    public void setDescriptor(Descriptor descriptor)
    {
        this.descriptor = descriptor;
    }

    public ArrayList<SSE> getSseVector()
    {
        return sseVector;
    }

    public void setSseVector(ArrayList<SSE> sseVector)
    {
        this.sseVector = sseVector;
    }

    public Sequence getSequence()
    {
        return sequence;
    }

    public void setSequence(Sequence sequence)
    {
        this.sequence = sequence;
    }

    public char getChainId()
    {
        return chainId;
    }

    public void setChainId(char chainId)
    {
        this.chainId = chainId;
    }

    public int getIntialAtom()
    {
        return intialAtom;
    }

    public void setIntialAtom(int intialAtom)
    {
        this.intialAtom = intialAtom;
    }

    public int getEndAtom()
    {
        return endAtom;
    }

    public void setEndAtom(int endAtom)
    {
        this.endAtom = endAtom;
    }

    public ArrayList<Integer> getResidueIndex()
    {
        return residueIndex;
    }

    public void setResidueIndex(ArrayList<Integer> residueIndex)
    {
        this.residueIndex = residueIndex;
    }

    public ArrayList<Integer> getResidueCAIndex()
    {
        return residueCAIndex;
    }

    public void setResidueCAIndex(ArrayList<Integer> residueCAIndex)
    {
        this.residueCAIndex = residueCAIndex;
    }

    public ArrayList<Coordinate> getResiduePointCA()
    {
        return residuePointCA;
    }

    public void setResiduePointCA(ArrayList<Coordinate> residuePointCA)
    {
        this.residuePointCA = residuePointCA;
    }

}
