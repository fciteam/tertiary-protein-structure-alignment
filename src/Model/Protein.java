/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Protein class holds all properties of the Protein data that have been read
 * form the PDB file in addition to informations that extended from this data ..
 *
 * @author Abdallah
 */
public class Protein
{

    /**
     * listOfChains : all listOfChains of the Protein ex: A, B, C..
     * numberOfChains : Number of chain in the Protein. proteinId Protein ID.
     */
    private ArrayList<Chain> listOfChains;
    private int numberOfChains;
    private String proteinId;
    private ArrayList<ArrayList<SecondaryStructure>> chainsSSE;

    /**
     * Default constructor
     */
    public Protein()
    {
        this.listOfChains = new ArrayList<>();
        this.chainsSSE = new ArrayList<>();
        this.proteinId = new String();
    }

    @Override
    public Object clone() throws CloneNotSupportedException
    {
        ArrayList<Chain> chainsTemp = new ArrayList<>();
        if (this.listOfChains.size() > 0)
        {
            for (int i = 0; i < this.numberOfChains; i++)
            {
                try
                {
                    chainsTemp.add((Chain) this.listOfChains.get(i).clone());
                }
                catch (CloneNotSupportedException ex)
                {
                    Logger.getLogger(Protein.class.getName()).log(Level.SEVERE,
                                                                  "This class is not clonebale!",
                                                                  ex);
                }
            }
        }
        return new Protein(chainsTemp, numberOfChains, proteinId);
    }

    /**
     * Parameterized constructor
     *
     * @param chains
     * @param numberOfChains
     * @param pdbId
     */
    public Protein(ArrayList<Chain> chains, int numberOfChains, String pdbId)
    {
        this.listOfChains = chains;
        this.numberOfChains = numberOfChains;
        this.proteinId = pdbId;
    }

    /**
     *
     * @return number of listOfChains in the Protein.
     */
    public int getNumberOfChains()
    {
        return numberOfChains;
    }

    /**
     * Change number of chains, This lead to clear and initialize
     * listOfChainsSSE lists.
     *
     * @param numberOfChains
     */
    public void setNumberOfChains(int numberOfChains)
    {
        this.numberOfChains = numberOfChains;
        for (int i = 0; i < this.numberOfChains; i++)
        {
            chainsSSE.add(new ArrayList<SecondaryStructure>());
        }
    }

    /**
     *
     * @return the list of listOfChains objects belong to this Protein.
     */
    public ArrayList<Chain> getListOfChains()
    {
        return listOfChains;
    }

    /**
     * Change listOfChains.
     *
     * @param listOfChains
     */
    public void setListOfChains(ArrayList<Chain> listOfChains)
    {
        this.listOfChains = listOfChains;
    }

    /**
     *
     * @return Protein ID.
     */
    public String getProteinId()
    {
        return proteinId;
    }

    /**
     * Change Protein ID.
     *
     * @param proteinId
     */
    public void setProteinId(String proteinId)
    {
        this.proteinId = proteinId;
    }

    /**
     * Add informations of chains and then calculate SSE
     *
     * @param atoms list of all CA atoms
     * @param helices list of all helices
     * @param sheets list of all sheets
     * @param sequences list of all sequences
     * @throws java.lang.Exception
     */
    public void process(ArrayList<Atom> atoms,
                        ArrayList<SecondaryStructure> helices,
                        ArrayList<SecondaryStructure> sheets,
                        ArrayList<Sequence> sequences) throws Exception
    {
        if (helices.isEmpty() && sheets.isEmpty())
        {
            throw new Exception("File without Helices and Sheets!");
        }
        setNumberOfChains(sequences.size());
        addSequenceInfo(sequences);
        setIntialEndAtom(atoms);
        setResidueIndexAndPoints(atoms);
        calculateSSE(helices, sheets, atoms);
    }

    /**
     * Initialize new chains and add it informations then link every sequence to
     * it chain.
     *
     * @param sequences list of all sequences
     */
    private void addSequenceInfo(ArrayList<Sequence> sequences)
    {
        for (Sequence sequence : sequences)
        {
            Chain chain = new Chain();
            chain.setChainId(sequence.getChainId());
            chain.setIntialAtom(100000);
            chain.setEndAtom(0);
            chain.setSequence(sequence);
            this.listOfChains.add(chain);
        }
    }

    /**
     *
     * @param chainId chain ID
     * @return index of chain that it ID = chainId in listOfChains, if not exist
     * will return -1.
     */
    public int getChainIndex(char chainId)
    {
        for (int i = 0; i < listOfChains.size(); i++)
        {
            if (listOfChains.get(i).getChainId() == chainId)
            {
                return i;
            }
        }
        return -1;
    }

    /*
     * //2 private void addAtomsInof(ArrayList<Atom> atoms) {//,
     * ArrayList<Character> listOfChains,ArrayList<Integer[]> boundreis) {
     *
     * ArrayList<ArrayList<Integer>> resIndex = new ArrayList<>(numberOfChains);
     * ArrayList<ArrayList<Integer>> resCaIndex = new ArrayList<>(
     * numberOfChains);
     *
     * for (int i = 0; i < numberOfChains; i++) { resIndex.add(new
     * ArrayList<Integer>()); resCaIndex.add(new ArrayList<Integer>()); }
     *
     * for (int i = 0; i < atoms.size(); i++) {
     *
     * for (int j = 0; j < this.getNumberOfChains(); j++) {
     *
     * if (this.listOfChains.get(j).getChainId() == atoms.get(i).getChainID()) {
     *
     * resIndex.get(j).add(atoms.get(i).getResidueSequenceNumber());
     * resCaIndex.get(j).add(atoms.get(i).getAtomSerialNumber()); if
     * (atoms.get(i).getAtomSerialNumber() < this.listOfChains.get(j)
     * .getIntialAtom()) { this.listOfChains.get(j).setIntialAtom(atoms.get(i).
     * getAtomSerialNumber()); } if (atoms.get(i).getAtomSerialNumber() >
     * this.listOfChains.get(j) .getEndAtom()) {
     * this.listOfChains.get(j).setEndAtom(atoms.get(i). getAtomSerialNumber());
     * } } } }
     *
     * for (int i = 0; i < this.getNumberOfChains(); i++) {
     *
     * this.listOfChains.get(i).setResidueIndex(resIndex.get(i));
     * this.listOfChains.get(i).setResidueCAIndex(resCaIndex.get(i)); /*
     * this.listOfChains.get(i).setResidueIndex(getRange(resin,
     * this.listOfChains.get(i).getIntialAtom(),
     * this.listOfChains.get(i).getEndAtom()));
     *
     * this.listOfChains.get(i).setResidueCAIndex(getRange(caind,
     * this.listOfChains.get(i).getIntialAtom(),
     * this.listOfChains.get(i).getEndAtom()));
     *
     * ArrayList<Coordinate> ca = new ArrayList<>(); for (int j = 0; j <
     * atoms.size(); j++) { if (atoms.get(j).getChainID() ==
     * this.listOfChains.get(i).getChainId()) { ca.add(new
     * Coordinate(atoms.get(j).getOrthogonal())); } }
     * this.listOfChains.get(i).setResiduePointCA(ca); } }
     *
     *
     * private ArrayList<Integer> getRange(ArrayList<Integer> arr, int s, int e)
     * { List<Integer> d = arr.subList(s, e); return new ArrayList<>(d); }
     */
    /**
     * initialize new SSE form secondaryStructure and return it.
     *
     * @param secondaryStructure helix or sheet
     * @param chainIndex index of chain in listOfChains
     * @param type type of secondaryStructure (H for helix or S for sheet)
     * @return new Object of SSE
     */
    private SSE fromSecondryStructureToSSE(SecondaryStructure secondaryStructure,
                                           int chainIndex, char type)
    {
        SSE sse = new SSE();
        sse.setChainIndex(chainIndex);
        sse.setIntialIndex(secondaryStructure.getInitial().
                getResidueSequenceNumber());
        sse.setEndIndex(secondaryStructure.getTerminal().
                getResidueSequenceNumber());
        sse.setCenteroid(secondaryStructure.getCenteriod());
        sse.setResultant(secondaryStructure.getResultant());
        sse.setSseClass(secondaryStructure.getSseClass());
        sse.setSseType(type);
        return sse;
    }

    /**
     * calculate SSE values for helices and sheets using CA atoms and then call
     * addSSE and processSSE function, to calculate all SSE requirements.
     *
     * @param helices list of all helices
     * @param sheets list of all sheets
     * @param atoms list of all CA atoms
     */
    private void calculateSSE(ArrayList<SecondaryStructure> helices,
                              ArrayList<SecondaryStructure> sheets,
                              ArrayList<Atom> atoms) throws Exception
    {
        calculateSSEValues(helices, atoms, 5);
        calculateSSEValues(sheets, atoms, 3);

        addSSEToSseVector();
        processSSE();
    }

    /**
     * Sort SecondaryStructures of chain index = chainIndex, in ascending by
     * ResidueSequenceNumber.
     *
     * @param chainIndex
     */
    private void sort(int chainIndex)
    {
        Collections.sort(this.chainsSSE.get(chainIndex),
                         new Comparator<SecondaryStructure>()
        {
            @Override
            public int compare(SecondaryStructure o1,
                               SecondaryStructure o2)
            {
                if (o1.getInitial().getResidueSequenceNumber() > o2.
                        getInitial()
                        .getResidueSequenceNumber())
                {
                    return 1;
                }
                else if (o1.getInitial().getResidueSequenceNumber() <
                         o2.
                        getInitial()
                        .getResidueSequenceNumber())
                {
                    return -1;
                }
                return 0;
            }
                         });
    }

    /**
     * Add SSE's to SSEVectors of every chains in listOfChains
     */
    private void addSSEToSseVector()
    {
        for (int i = 0; i < this.chainsSSE.size(); i++)
        {
            sort(i);
            for (int j = 0; j < this.chainsSSE.get(i).size(); j++)
            {
                this.listOfChains.get(i).getSseVector().add(
                        fromSecondryStructureToSSE(
                                this.chainsSSE.get(i).get(j), i, this.chainsSSE.
                                get(i).get(j).getSseType()));
            }
        }
    }

    /*
     * private void setChainsSSE(ArrayList<SecondaryStructure> ss, char c) { for
     * (SecondaryStructure s : ss) { if (s != null) { for (int j = 0; j <
     * listOfChains.size(); j++) {
     *
     * if (s.getInitial().getChainIdentifier() ==
     * listOfChains.get(j).getChainId()) {
     * listOfChains.get(j).getSseVector().add(fromSecondryStructureToSSE(s, j,
     * c)); }
     *
     * }
     * }
     * }
     * }
     */
    /**
     * Linking every Helix to its atoms
     *
     * @param secondaryStructure helices or sheets list
     * @param atoms list of atoms belong to the secondaryStructures
     * @param num
     */
    private void calculateSSEValues(
            ArrayList<SecondaryStructure> secondaryStructure,
            ArrayList<Atom> atoms, int num) throws Exception
    {
        for (int i = 0; i < secondaryStructure.size(); i++)
        {
            boolean isTaken = false;
            for (int j = 0; j < numberOfChains; j++)
            {
                if (secondaryStructure.get(i).getInitial().getChainIdentifier() ==
                    this.listOfChains.get(j).getChainId() &&
                    secondaryStructure.get(i).getTerminal().
                        getChainIdentifier() ==
                    this.listOfChains.get(j).getChainId())
                {
                    int startResidueSequenceNumber = secondaryStructure.get(i).
                            getInitial().getResidueSequenceNumber();
                    int endResidueSequenceNumber = secondaryStructure.get(i).
                            getTerminal().getResidueSequenceNumber();

                    if (endResidueSequenceNumber - startResidueSequenceNumber +
                        1 >= num)
                    {
                        ArrayList<Atom> atomsRange = null;
                        atomsRange = getRangeOfAtoms(atoms,
                                                     startResidueSequenceNumber,
                                                     endResidueSequenceNumber,
                                                     secondaryStructure.get(i).
                                getInitial().
                                getChainIdentifier());

                        if (atomsRange != null && atomsRange.size() >= num)
                        {
                            secondaryStructure.get(i).setAtoms(atomsRange);
                            /**
                             * Added by abdallah to solve start & end Resd
                             * problem that will occur in Alignment ..
                             */
                            secondaryStructure.get(i).getInitial().
                                    setResidueSequenceNumber(
                                            atomsRange.get(0).
                                            getResidueSequenceNumber());
                            secondaryStructure.get(i).getTerminal().
                                    setResidueSequenceNumber(
                                            atomsRange.
                                            get(atomsRange.size() - 1).
                                            getResidueSequenceNumber());
                            /**
                             * ***************
                             */
                            secondaryStructure.get(i).calculateCoordinates();
                            SecondaryStructure ssCopy = null;
                            ssCopy = (SecondaryStructure) secondaryStructure.
                                    get(
                                            i).clone();
                            chainsSSE.get(j).add(ssCopy);
                            isTaken = true;
                        }
                        else
                        {
                            //System.out.println(
                            //"Number of CA atoms for secondaryStructure["
                            //+ j
                            //+ "][" + i + "] are not enough!");
                        }
                    }
                    else
                    {
                        //System.out.println(
                        //"Number of CA atoms for secondaryStructure[" + j
                        //+ "][" + i + "] are not enough!");
                    }
                }
            }

            if (!isTaken)
            {
                secondaryStructure.set(i, null);
            }
        }
    }

    /**
     * Get list of atoms indeed range startIndex to endIndex
     *
     * @param atoms
     * @param startIndex
     * @param endIndex
     * @return subList of atoms from startIndex to endIndex.
     */
    private ArrayList<Atom> getRangeOfAtoms(ArrayList<Atom> atoms,
                                            int startIndex, int endIndex, char c)
            throws Exception
    {
        ArrayList<Atom> list = new ArrayList<>();
        for (Atom atom : atoms)
        {
            if (atom.getChainID() == c && atom.getResidueSequenceNumber() >=
                                          startIndex && atom.
                    getResidueSequenceNumber() <= endIndex)
            {
                list.add(atom);
            }
            else if (atom.getResidueSequenceNumber() > endIndex)
            {
                break;
            }
        }
        return list;
        /*
         * int Startindedex = atoms.indexOf(new Atom(startIndex, c)); int
         * Endindedex = atoms.indexOf(new Atom(endIndex, c)); try { list =
         * atoms.subList(Startindedex, Endindedex + 1); } catch (Exception ex) {
         * throw new Exception( "Out of range of atoms list in atomsRange
         * function!"); } return new ArrayList<>(list);
         */
    }

    /*
     * private ArrayList<Atom> getRangeOfAtoms(ArrayList<Atom> atoms, int start,
     * int end) { for (int i = 0; i < atoms.size(); i++) { if
     * (atoms.get(i).getAtomSerialNumber() == start) { start = i; break; } } for
     * (int i = 0; i < atoms.size(); i++) { if
     * (atoms.get(i).getAtomSerialNumber() == end) { end = i; break; } }
     * List<Atom> l = atoms.subList(start, end); return new ArrayList<>(l); }
     */
    /**
     * Apply process SSE on all chains.
     */
    private void processSSE()
    {
        for (int i = 0; i < this.getNumberOfChains(); i++)
        {
            processSSE(i);
        }
    }

    /**
     * Apply process SSE in specific chains by index
     *
     * @param index
     */
    public void processSSE(int index)
    {
        int[] dir;
        for (int j = 0; j < listOfChains.get(index).getSseVector().size() - 1;
             j++)
        {
            Coordinate centeriodCurrent =
                       listOfChains.get(index).getSseVector().
                    get(
                            j).getCenteroid();
            Coordinate resultant =
                       listOfChains.get(index).getSseVector().get(j).
                    getResultant();
            Coordinate centeriodNext = listOfChains.get(index).getSseVector().
                    get(
                            j + 1).getCenteroid();
            Coordinate interdirection = new Coordinate();
            interdirection.substruct(centeriodNext, centeriodCurrent);

            listOfChains.get(index).getDescriptor().setTypes(listOfChains.get(
                    index).getDescriptor().getTypes() +
                                                             listOfChains.get(
                    index).getSseVector().get(j).getSseType());

            dir = resultant.getVectorDirection();
            listOfChains.get(index).getDescriptor().getResultantsSymbols().add(
                    (int) dir[0]);
            listOfChains.get(index).getDescriptor().getResultantsTheta().add(
                    (int) dir[1]);
            listOfChains.get(index).getDescriptor().getResultantsPhi().add(
                    (int) dir[2]);

            dir = interdirection.getVectorDirection();
            listOfChains.get(index).getDescriptor().getInterdirectionsSymbols().
                    add(
                            (int) dir[0]);
            listOfChains.get(index).getDescriptor().getInterdirectionsTheta().
                    add(
                            (int) dir[1]);
            listOfChains.get(index).getDescriptor().getInterdirectionsPhi().add(
                    (int) dir[2]);
            listOfChains.get(index).getDescriptor().getProximity().add(dir[3]);
        }

        if (listOfChains.get(index).getSseVector().size() > 0)
        {
            Coordinate resultant = listOfChains.get(index).getSseVector().get(
                    listOfChains.get(index).getSseVector().size() - 1).
                    getResultant();
            dir = resultant.getVectorDirection();
            listOfChains.get(index).getDescriptor().getResultantsSymbols().add(
                    (int) dir[0]);
            listOfChains.get(index).getDescriptor().getResultantsTheta().add(
                    (int) dir[1]);
            listOfChains.get(index).getDescriptor().getResultantsPhi().add(
                    (int) dir[2]);

            listOfChains.get(index).getDescriptor().setTypes(
                    listOfChains.get(index).getDescriptor().
                    getTypes() + listOfChains.get(index).getSseVector()
                    .get(listOfChains.get(index).getSseVector().size() - 1).
                    getSseType());
        }
    }

    /**
     * Set initial and end atoms of every chain
     *
     * @param atoms list of all atom
     */
    public void setIntialEndAtom(ArrayList<Atom> atoms)
    {
        for (int i = 0; i < atoms.size(); i++)
        {
            for (int j = 0; j < listOfChains.size(); j++)
            {
                if (atoms.get(i).getChainID() == listOfChains.get(j).
                        getChainId())
                {
                    if (i < listOfChains.get(j).getIntialAtom())
                    {
                        listOfChains.get(j).setIntialAtom(i);
                    }
                    if (i > listOfChains.get(j).getEndAtom())
                    {
                        listOfChains.get(j).setEndAtom(i);
                    }
                    break;
                }
            }
        }
    }

    /**
     * Get points of atoms between start and end index
     *
     * @param atoms list of atoms
     * @param startIndex start index
     * @param endIndex end index
     * @return array of pair (ResidueSequenceNumber, Orthogonal)
     */
    public Object[] getResuduIndexAndPoints(ArrayList<Atom> atoms,
                                            int startIndex, int endIndex)
    {
        ArrayList<Integer> resInd = new ArrayList<>();
        ArrayList<Coordinate> points = new ArrayList<>();

        for (int i = startIndex; i <= endIndex; i++)
        {
            resInd.add(atoms.get(i).getResidueSequenceNumber());
            points.add(atoms.get(i).getOrthogonal());
        }

        return new Object[]
        {
            resInd, points
        };
    }

    /**
     * Set Residue Index And Points to list of chains
     *
     * @param atoms list atoms
     */
    public void setResidueIndexAndPoints(ArrayList<Atom> atoms)
    {
        for (int i = 0; i < listOfChains.size(); i++)
        {
            Object[] tempOb = getResuduIndexAndPoints(atoms,
                                                      listOfChains.get(i).
                    getIntialAtom(),
                                                      listOfChains.get(i).
                    getEndAtom());
            listOfChains.get(i).setResidueIndex((ArrayList<Integer>) tempOb[0]);
            listOfChains.get(i).setResiduePointCA(
                    (ArrayList<Coordinate>) tempOb[1]);
        }
    }

    /**
     * Get atom index searching by ResidueSequenceNumber
     *
     * @param atoms list of atoms
     * @param residueSequenceNumber Residue Sequence Number (initial or end)
     * @return atom index
     */
    public int getAtomIndex(ArrayList<Atom> atoms, int residueSequenceNumber)
    {
        for (int i = 0; i < atoms.size(); i++)
        {
            if (atoms.get(i).getResidueSequenceNumber() == residueSequenceNumber)
            {
                return i + 1;
            }
        }
        return 0;
    }

    public Chain getSpecificChain(char c)
    {

        int index = this.getChainIndex(c);
        return listOfChains.get(index);

    }

    public Protein clean()
    {
        Protein p = new Protein();
        p.setProteinId(proteinId);
        int k = 0;
        for (int i = 0; i < listOfChains.size(); i++)
        {
            if (!listOfChains.get(i).getDescriptor().getTypes().isEmpty())
            {
                p.getListOfChains().add(listOfChains.get(i));
                p.getChainsSSE().add(chainsSSE.get(i));
                k++;
            }
        }
        p.setNumberOfChains(k);
        return p;
    }

    public ArrayList<ArrayList<SecondaryStructure>> getChainsSSE()
    {
        return chainsSSE;
    }

    public void setChainsSSE(
            ArrayList<ArrayList<SecondaryStructure>> chainsSSE)
    {
        this.chainsSSE = chainsSSE;
    }
}
