package Model;

import java.util.ArrayList;

/**
 * @author Abdallah
 */
public class SecondaryStructure implements Cloneable
{

    protected int serialNumber;
    protected String identifier;
    protected Residue initial;
    protected Residue terminal;
    protected ArrayList<Atom> atoms;
    protected Coordinate start;
    protected Coordinate end;
    protected Coordinate centeriod;
    protected Coordinate resultant;
    protected int sseClass;
    protected char sseType;

    public char getSseType()
    {
        return sseType;
    }

    public void setSseType(char sseType)
    {
        this.sseType = sseType;
    }

    public int getSseClass()
    {
        return sseClass;
    }

    public void setSseClass(int sseClass)
    {
        this.sseClass = sseClass;
    }

    public void setAtoms(ArrayList<Atom> atoms)
    {
        this.atoms = atoms;
    }

    public Coordinate getResultant()
    {
        return resultant;
    }

    public void setResultant(Coordinate resultant)
    {
        this.resultant = resultant;
    }

    /**
     *
     */
    public SecondaryStructure()
    {
        this.serialNumber = 0;
        this.identifier = new String();
        this.initial = new Residue();
        this.terminal = new Residue();
        this.atoms = new ArrayList<>();
        this.start = new Coordinate();
        this.end = new Coordinate();
        this.centeriod = new Coordinate();
        this.sseClass = 0;
    }

    public SecondaryStructure(int serialNumber, String identifier,
                              Residue initial, Residue terminal,
                              ArrayList<Atom> atoms, Coordinate start,
                              Coordinate end, Coordinate centeriod,
                              Coordinate resultant, int sseClass, char type)
    {
        this.serialNumber = serialNumber;
        this.identifier = identifier;
        this.initial = initial;
        this.terminal = terminal;
        this.atoms = atoms;
        this.start = start;
        this.end = end;
        this.centeriod = centeriod;
        this.resultant = resultant;
        this.sseClass = sseClass;
        this.sseType = type;
    }

    public SecondaryStructure(int serialNumber, String identifier,
                              Residue initial, Residue terminal,
                              ArrayList<Atom> atoms, Coordinate start,
                              Coordinate end, Coordinate centeriod,
                              Coordinate resultant, int sseClass)
    {
        this.serialNumber = serialNumber;
        this.identifier = identifier;
        this.initial = initial;
        this.terminal = terminal;
        this.atoms = atoms;
        this.start = start;
        this.end = end;
        this.centeriod = centeriod;
        this.resultant = resultant;
        this.sseClass = sseClass;
    }

    public SecondaryStructure(int serialNumber, String identifier,
                              Residue initial, Residue terminal,
                              ArrayList<Atom> atoms, Coordinate start,
                              Coordinate end, Coordinate center)
    {
        this.serialNumber = serialNumber;
        this.identifier = identifier;
        this.initial = initial;
        this.terminal = terminal;
        this.atoms = atoms;
        this.start = start;
        this.end = end;
        this.centeriod = center;
    }

    /**
     * @param serialNumber
     * @param identifier
     * @param initial
     * @param terminal
     */
    public SecondaryStructure(int serialNumber, String identifier,
                              Residue initial,
                              Residue terminal)
    {
        super();
        this.serialNumber = serialNumber;
        this.identifier = identifier;
        this.initial = initial;
        this.terminal = terminal;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<Atom> getAtoms()
    {
        return atoms;
    }

    /**
     * @return
     */
    public int getSerialNumber()
    {
        return serialNumber;
    }

    /**
     * @return
     */
    public String getIdentifier()
    {
        return identifier;
    }

    /**
     * @return
     */
    public Residue getInitial()
    {
        return initial;
    }

    /**
     * @return
     */
    public Residue getTerminal()
    {
        return terminal;
    }

    /**
     * @param serialNumber
     */
    public void setSerialNumber(int serialNumber)
    {
        this.serialNumber = serialNumber;
    }

    /**
     * @param identifier
     */
    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    /**
     * @param initial
     */
    public void setInitial(Residue initial)
    {
        this.initial = initial;
    }

    /**
     * @param terminal
     */
    public void setTerminal(Residue terminal)
    {
        this.terminal = terminal;
    }

    public void extractAtoms(ArrayList<Atom> list)
    {
        for (int i = 0; i < list.size(); i++)
        {
            if (list.get(i).getResidueSequenceNumber() >= initial.
                    getResidueSequenceNumber() &&
                list.get(i).getResidueSequenceNumber() <= terminal.
                    getResidueSequenceNumber())
            {
                atoms.add(list.get(i));
            }
            else if (list.get(i).getResidueSequenceNumber() > terminal.
                    getResidueSequenceNumber())
            {
                break;
            }
        }
    }

    public void calculateCoordinates()
    {
        start = new Coordinate();
        end = new Coordinate();
        centeriod = new Coordinate();
        resultant = new Coordinate();
        double x, y, z;

        if (this instanceof Helix)
        {
            x = ((.74 * getAtoms().get(0).getOrthogonal().getX()) +
                 getAtoms().get(1).getOrthogonal().getX() + (.74 *
                                                             getAtoms().get(3).
                                                             getOrthogonal().
                                                             getX())) / 3.48;

            y = ((.74 * getAtoms().get(0).getOrthogonal().getY()) +
                 getAtoms().get(1).getOrthogonal().getY() + (.74 *
                                                             getAtoms().get(3).
                                                             getOrthogonal().
                                                             getY())) / 3.48;

            z = ((.74 * getAtoms().get(0).getOrthogonal().getZ()) +
                 getAtoms().get(1).getOrthogonal().getZ() + (.74 *
                                                             getAtoms().get(3).
                                                             getOrthogonal().
                                                             getZ())) / 3.48;

            start.setX(x);
            start.setY(y);
            start.setZ(z);

            int size = atoms.size();

            x = ((.74 * getAtoms().get(size - 4).getOrthogonal().getX()) +
                 getAtoms().get(size - 3).getOrthogonal().getX() + (.74 *
                                                                    getAtoms().
                                                                    get(size -
                                                                        1).
                                                                    getOrthogonal().
                                                                    getX())) /
                3.48;

            y = ((.74 * getAtoms().get(size - 4).getOrthogonal().getY()) +
                 getAtoms().get(size - 3).getOrthogonal().getY() + (.74 *
                                                                    getAtoms().
                                                                    get(size -
                                                                        1).
                                                                    getOrthogonal().
                                                                    getY())) /
                3.48;

            z = ((.74 * getAtoms().get(size - 4).getOrthogonal().getZ()) +
                 getAtoms().get(size - 3).getOrthogonal().getZ() + (.74 *
                                                                    getAtoms().
                                                                    get(size -
                                                                        1).
                                                                    getOrthogonal().
                                                                    getZ())) /
                3.48;

            end.setX(x);
            end.setY(y);
            end.setZ(z);

            centeriod = calculateCenteriod(atoms);
        }
        else if (this instanceof Sheet)
        {
            if (atoms.size() == 3)
            {
                start.setX(atoms.get(0).getOrthogonal().getX());
                start.setY(atoms.get(0).getOrthogonal().getY());
                start.setZ(atoms.get(0).getOrthogonal().getZ());

                centeriod.setX(atoms.get(1).getOrthogonal().getX());
                centeriod.setY(atoms.get(1).getOrthogonal().getY());
                centeriod.setZ(atoms.get(1).getOrthogonal().getZ());

                end.setX(atoms.get(2).getOrthogonal().getX());
                end.setY(atoms.get(2).getOrthogonal().getY());
                end.setZ(atoms.get(2).getOrthogonal().getZ());

            }
            else
            {
                x = ((getAtoms().get(0).getOrthogonal().getX()) +
                     getAtoms().get(1).getOrthogonal().getX()) / 2.0;

                y = ((getAtoms().get(0).getOrthogonal().getY()) +
                     getAtoms().get(1).getOrthogonal().getY()) / 2.0;

                z = ((getAtoms().get(0).getOrthogonal().getZ()) +
                     getAtoms().get(1).getOrthogonal().getZ()) / 2.0;

                start.setX(x);
                start.setY(y);
                start.setZ(z);

                int size = atoms.size();

                x = ((getAtoms().get(size - 2).getOrthogonal().getX()) +
                     getAtoms().get(size - 1).getOrthogonal().getX()) / 2.0;

                y = ((getAtoms().get(size - 2).getOrthogonal().getY()) +
                     getAtoms().get(size - 1).getOrthogonal().getY()) / 2.0;

                z = ((getAtoms().get(size - 2).getOrthogonal().getZ()) +
                     getAtoms().get(size - 1).getOrthogonal().getZ()) / 2.0;

                end.setX(x);
                end.setY(y);
                end.setZ(z);

                centeriod = calculateCenteriod(atoms);
            }
        }
        resultant.substruct(end, start);
    }

    public Coordinate calculateCenteriod(ArrayList<Atom> atom)
    {
        Coordinate ce = new Coordinate();

        for (Atom t : atom)
        {
            ce.setX(ce.getX() + t.getOrthogonal().getX());
            ce.setY(ce.getY() + t.getOrthogonal().getY());
            ce.setZ(ce.getZ() + t.getOrthogonal().getZ());
        }

        ce.setX(ce.getX() / atom.size());
        ce.setY(ce.getY() / atom.size());
        ce.setZ(ce.getZ() / atom.size());

        return ce;
    }

    public Coordinate getStart()
    {
        return start;
    }

    public void setStart(Coordinate start)
    {
        this.start = start;
    }

    public Coordinate getEnd()
    {
        return end;
    }

    public void setEnd(Coordinate end)
    {
        this.end = end;
    }

    public Coordinate getCenteriod()
    {
        return centeriod;
    }

    public void setCenteriod(Coordinate centeriod)
    {
        this.centeriod = centeriod;
    }
}
