/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import org.jblas.DoubleMatrix;

/**
 * @author ahmed
 */
public class Matrix extends DoubleMatrix
{

    public static Matrix toParent(DoubleMatrix m)
    {
        Matrix reMatrix = new Matrix(m.getRows(), m.getColumns());
        for (int i = 0; i < m.getRows(); i++)
        {
            for (int j = 0; j < m.getColumns(); j++)
            {
                reMatrix.put(i, j, m.get(i, j));
            }
        }
        return reMatrix;
    }

    /**
     * Initialize M*N matrix of 0's
     *
     * @param m number of rows
     * @param n number of columns
     */
    public Matrix(int m, int n)
    {
        super(m, n);
    }

    /**
     * Initialize matrix given 2D array
     *
     * @param data 2D array
     */
    public Matrix(double[][] data)
    {
        super(data);
    }

    /**
     * Copy constructor
     *
     * @param a DoubleMatrix
     */
    public Matrix(DoubleMatrix a)
    {
        super(a.toArray2());
    }

    /**
     * Is this equal to b?
     *
     * @param b
     * @return boolean
     */
    public boolean eq(Matrix b)
    {
        return super.equals(b);
    }

    /**
     * create and return the n identity matrix
     *
     * @param n number of rows = columns
     * @return Matrix
     */
    public static Matrix identity(int n)
    {
        Matrix I = new Matrix(n, n);

        for (int i = 0; i < n; i++)
        {
            I.put(i, i, 1);
        }
        return I;
    }

    /**
     * Convert Arraylist of Coordinates to Matrix
     *
     * @param list
     * @return Matrix
     */
    public static Matrix fromCoordinatesToMatrix(ArrayList<Coordinate> list)
    {
        Matrix m = new Matrix(3, list.size());
        for (int i = 0; i < list.size(); i++)
        {
            m.put(0, i, list.get(i).getX());
            m.put(1, i, list.get(i).getY());
            m.put(2, i, list.get(i).getZ());
        }
        return m;
    }

    /**
     * Add two matrices
     *
     * @param m1
     * @param m2
     * @return Matrix
     */
    public static Matrix add(Matrix m1, Matrix m2)
    {
        Matrix r = new Matrix(m1.rows, m1.columns);
        for (int i = 0; i < r.rows; i++)
        {
            for (int j = 0; j < r.columns; j++)
            {
                r.put(i, j, m1.get(i, j) + m2.get(i, j));
            }
        }
        return r;
    }

    /**
     * Generate Arraylist of Coordinates from the data
     *
     * @return ArrayList<Coordinate>
     */
    public ArrayList<Coordinate> toCoordinates()
    {
        ArrayList<Coordinate> coordinates = new ArrayList<>();
        for (int i = 0; i < columns; i++)
        {
            coordinates.add(new Coordinate(get(0, i), get(1, i), get(2, i)));
        }
        return coordinates;
    }

    /**
     * Square root of the matrix
     *
     * @return Matrix
     */
    public Matrix sqrt()
    {
        Matrix newMat = new Matrix(rows, columns);
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                newMat.put(i, j, Math.sqrt(get(i, j)));
            }
        }
        return newMat;
    }

    /**
     * Calculate determinant of the 2D array
     *
     * @param matrix 2D array
     * @return double
     */
    public static double determinant(double[][] matrix)
    {
        return determinant(matrix, matrix.length);
    }

    private static double determinant(double[][] matrix, int n)
    {
        double det = 0;
        int sign = 1, p, q;

        if (n == 1)
        {
            det = matrix[0][0];
        }
        else
        {
            double[][] b = new double[n - 1][n - 1];
            for (int x = 0; x < n; x++)
            {
                p = 0;
                q = 0;
                for (int i = 1; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (j != x)
                        {
                            b[p][q++] = matrix[i][j];
                            if (q % (n - 1) == 0)
                            {
                                p++;
                                q = 0;
                            }
                        }
                    }
                }
                det = det + matrix[0][x] * determinant(b, n - 1) * sign;
                sign = -sign;
            }
        }
        return det;
    }

    /**
     * Repeat column n times
     *
     * @param n times
     * @return Matrix
     */
    public Matrix repeat(int n)
    {
        Matrix newMat = new Matrix(rows, n);
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < n; j++)
            {
                newMat.put(i, j, get(i, 0));
            }
        }
        return newMat;
    }

    public static DoubleMatrix divOnScaller(int i, DoubleMatrix doubleMatrix)
    {
        DoubleMatrix resulMatrix =
                     new DoubleMatrix(doubleMatrix.getRows(), doubleMatrix.
                getColumns());
        for (int j = 0; j < 10; j++)
        {
            for (int k = 0; k < 10; k++)
            {
                resulMatrix.put(j, k, i / doubleMatrix.get(j, k));
            }
        }
        return resulMatrix;
    }

    public double calcukateTMScore(int n1)
    {
        double sum = 0;
        for (int i = 0; i < getRows(); i++)
        {
            for (int j = 0; j < getColumns(); j++)
            {
                sum += (1.0 / (1 + get(i, j) * get(i, j) /
                                   (1.24 * Math.pow(n1 * 1.0 - 15, 1.0 / 3.0) -
                                    1.8)));
            }
        }
        sum = sum / n1;
        return sum;
    }
}
