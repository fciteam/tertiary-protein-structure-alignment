package Viewer;

//~--- non-JDK imports --------------------------------------------------------
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.biojava.bio.structure.Structure;
import org.biojava.bio.structure.io.PDBFileReader;
import org.jmol.adapter.smarter.SmarterJmolAdapter;
import org.jmol.api.JmolAdapter;
import org.jmol.api.JmolSimpleViewer;
import org.jmol.export.dialog.FileChooser;

/**
 *
 * @author PC
 */
public final class Viewer extends JFrame
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private JmolSimpleViewer viewer;
    private final Viewer parent;

    // private Structure structure;
    private final JmolPanel jmolPanel;
    private String PDBID;
    public Viewer()
    {
        setTitle("PDB Viewer");
        // this.addWindowListener(new ApplicationCloser());
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setIconImage(new ImageIcon(getClass().getResource("icon.png")).
                getImage());
        addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                JFrame frame = (JFrame) e.getSource();
                int result = JOptionPane.showConfirmDialog(frame,
                                                           "Are you sure you want to exit the application?",
                                                           "Exit Application",
                                                           JOptionPane.YES_NO_OPTION);

                if (result == JOptionPane.YES_OPTION)
                {
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                }
            }
        });
        addKeyListener(new KeyListener()
        {
            @Override
            public void keyTyped(KeyEvent arg0)
            {

                // TODO Auto-generated method stub
            }

            @Override
            public void keyReleased(KeyEvent arg0)
            {

                // TODO Auto-generated method stub
            }

            @Override
            public void keyPressed(KeyEvent e)
            {
                int key = e.getKeyCode();

                if ((key == KeyEvent.VK_MINUS) && ((e.getModifiers() &
                                                    KeyEvent.CTRL_MASK) != 0))
                {
                    evulateString("zoom 50;");
                }

                if ((key == 61) &&
                    ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0))
                {
                    evulateString("zoom 50;");
                }
            }
        });
        setJMenuBar(createMenu());

        setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
        this.pack();

        //   parent = this;
        Container contentPane = getContentPane();

        jmolPanel = new JmolPanel();
        jmolPanel.setPreferredSize(new Dimension(200, 200));
        contentPane.add(jmolPanel);

        //setUndecorated(true);
        // ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        pack();

        setSize(800, 600);
        setLocationRelativeTo(null);
        setVisible(true);
        parent = this;
    }
    /**
     *
     * @param struc
     */
    public Viewer(Structure struc)
    {
        this();
        setStructure(struc);
    }

    public final native Class<?> getMyType();

    /**
     *
     * @param struct
     */
    public void setStructure(Structure struct)
    {
        if (struct != null)
        {

            String pdb = struct.toPDB();
            // structure = s;
            PDBID = struct.getName();
            JmolSimpleViewer tmpviewer = jmolPanel.getViewer();

            tmpviewer.openStringInline(pdb);
            tmpviewer.evalString("select *; spacefill off; wireframe off; ");    // backbone 0.4;  ");

            // viewer.evalString("color secondary structure;");
            tmpviewer.evalString("cartoon;");
            tmpviewer.evalString(
                    "color green;select *;color cartoons structure;color " +
                    "rockets chain;color backbone blue;");

            // viewer.evalFile("color cartoons structure;");
            tmpviewer.evalString("zoom 50;");
            tmpviewer.evalString("spin on;");
            tmpviewer.evalString("axes on;");
            this.viewer = tmpviewer;

        }
    }

    /**
     *
     * @return
     */
    public JmolSimpleViewer getViewer()
    {
        return jmolPanel.getViewer();
    }

    /**
     *
     * @param query
     */
    public void evulateString(String query)
    {
        if (this.viewer != null)
        {
            this.viewer.evalString(query);
        }
    }

    void setStructure(Structure struct, String file)
    {
        if (struct != null)
        {
            setName(struct.getPDBCode());
            try
            {
                this.setTitle(file);

            }
            catch (Exception e)
            {
            }
            setStructure(struct);
        }
    }

    private JMenuBar createMenu()
    {
        JMenuBar menuBar = new JMenuBar();

        JMenu theme = new JMenu("View");
        JMenu metal = new JMenu("Metal");
        metal.setHorizontalTextPosition(SwingConstants.CENTER);
        metal.setVerticalTextPosition(SwingConstants.BOTTOM);

        JMenuItem defaultMetal = new JMenuItem("Default Metal");
        defaultMetal.setHorizontalTextPosition(SwingConstants.CENTER);
        defaultMetal.setVerticalTextPosition(SwingConstants.BOTTOM);
        defaultMetal.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                Controller.LOOKANDFEEL = "Metal";
                Controller.THEME = "DefaultMetal";
                Controller.initLookAndFeel();
            }
        });
        metal.add(defaultMetal);

        JMenuItem ocean = new JMenuItem("Ocean");
        ocean.setHorizontalTextPosition(SwingConstants.CENTER);
        ocean.setVerticalTextPosition(SwingConstants.BOTTOM);
        ocean.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                Controller.LOOKANDFEEL = "Metal";
                Controller.THEME = "Ocean";
                Controller.initLookAndFeel();
            }
        });

        metal.add(ocean);

        JMenuItem system = new JMenuItem("System");
        system.setHorizontalTextPosition(SwingConstants.CENTER);
        system.setVerticalTextPosition(SwingConstants.BOTTOM);
        system.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                Controller.LOOKANDFEEL = "System";
                Controller.initLookAndFeel();
            }
        });

        JMenuItem nimbus = new JMenuItem("Nimbus");
        nimbus.setHorizontalTextPosition(SwingConstants.CENTER);
        nimbus.setVerticalTextPosition(SwingConstants.BOTTOM);
        nimbus.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                Controller.LOOKANDFEEL = "Nimbus";
                Controller.initLookAndFeel();
            }
        });

        JMenuItem motif = new JMenuItem("Motif");
        motif.setHorizontalTextPosition(SwingConstants.CENTER);
        motif.setVerticalTextPosition(SwingConstants.BOTTOM);
        motif.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                Controller.LOOKANDFEEL = "Motif";
                Controller.initLookAndFeel();
            }
        });

        /*
         * JMenuItem gtk = new JMenuItem("GTK");
         * gtk.setHorizontalTextPosition(SwingConstants.CENTER);
         * gtk.setVerticalTextPosition(SwingConstants.BOTTOM);
         * gtk.addActionListener(new ActionListener() { @Override public void
         * actionPerformed(ActionEvent e) { Controller.LOOKANDFEEL = "GTK";
         * Controller.initLookAndFeel(); } });
         */
        theme.add(metal);
        theme.add(system);
        theme.add(motif);
        // theme.add(gtk);
        theme.add(nimbus);

        JMenu menu = new JMenu("File");

        // BufferedImage image = ImageIO.read(new URL("http://pscode.org/media/stromlo1.jpg"));
        menu.setHorizontalTextPosition(SwingConstants.CENTER);
        menu.setVerticalTextPosition(SwingConstants.BOTTOM);

        JMenuItem open = new JMenuItem("Open");
        open.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser open = new FileChooser();
                int res = open.showOpenDialog(parent);

                if (res == JOptionPane.YES_OPTION)
                {
                    String file = open.getSelectedFile().getPath().toString();
                    PDBFileReader reader = new PDBFileReader();
                    reader.setPath(file);
                    try
                    {
                        Structure struct = reader.getStructure(file);
                        setStructure(struct, file
                        );
                    }
                    catch (IOException ex)
                    {
                        Logger.getLogger(Viewer.class.getName()).log(
                                Level.SEVERE, null, ex);
                    }
                }
            }
        });
        menu.add(open);
        JMenuItem print = new JMenuItem("Print");
        print.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                printComponenet();
            }

            public void printComponenet()
            {

                PrinterJob pj = PrinterJob.getPrinterJob();
                pj.setJobName(" Print PDB ");
                final JmolPanel componenet = jmolPanel;
                pj.setPrintable(new Printable()
                {
                    @Override
                    public int print(Graphics pg, PageFormat pf, int pageNum)
                    {
                        if (pageNum > 0)
                        {
                            return Printable.NO_SUCH_PAGE;
                        }

                        Graphics2D g2 = (Graphics2D) pg;
                        g2.translate(pf.getImageableX(), pf.getImageableY());
                        evulateString("spin off;");
                        evulateString("axes off;");
                        evulateString("zoom 65;");
                        //evulateString("color background white;");
                        componenet.paint(g2);
                        evulateString("zoom 50;");
                        // evulateString("color background black;");
                        evulateString("spin on;");
                        evulateString("axes on;");
                        return Printable.PAGE_EXISTS;
                    }
                });

                if (pj.printDialog() == false)
                {
                    return;
                }

                try
                {
                    pj.print();
                }
                catch (PrinterException ex)
                {
                    // handle exception
                }
            }
        });
        menu.add(print);
        JMenuItem save = new JMenuItem("Save as Image");
        save.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                saveImage();
            }

            private void saveImage()
            {
                try
                {
                    JFileChooser save = new FileChooser();
                    FileFilter imageFilter = new FileNameExtensionFilter(
                            "Image files", ImageIO.getReaderFileSuffixes());
                    save.setSelectedFile(new File(PDBID + ".jpg"));
                    save.setFileFilter(imageFilter);
                    int choise = save.showSaveDialog(parent);
                    if (choise == JFileChooser.APPROVE_OPTION)
                    {
                        String fName = save.getSelectedFile().toString();
                        evulateString("spin off;");
                        evulateString("axes off;");
                        evulateString("zoom 65;");
                                               // evulateString("color background white;");
                        BufferedImage img = createImage(jmolPanel);
                        // evulateString("color background black;");
                        evulateString("zoom 50;");
                        evulateString("spin on;");
                        evulateString("axes on;");
                        int index = fName.lastIndexOf(".");
                        String ext;
                        if (index == -1)
                        {
                            ext = ImageIO.getReaderFileSuffixes()[0];
                            fName += "." + ext;
                        }
                        else
                        {
                            ext = fName.toString().
                                    substring(fName.lastIndexOf(".") + 1);
                        }
                        ImageIO.write(img, ext, new File(fName));

                    }

                }
                catch (HeadlessException | IOException ex)
                {
                }
            }

            public BufferedImage createImage(JPanel panel)
            {

                int w = panel.getWidth();
                int h = panel.getHeight();
                BufferedImage bi = new BufferedImage(w, h,
                                                     BufferedImage.TYPE_INT_RGB);
                Graphics2D g = bi.createGraphics();
                panel.paint(g);
                return bi;
            }

        });
        menu.add(save);
        JMenuItem close = new JMenuItem("Exit");
        close.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

                Viewer w = parent;

                w.getToolkit().getSystemEventQueue().postEvent(
                        new WindowEvent(w, WindowEvent.WINDOW_CLOSING));
            }
        });
        menu.add(close);
        menuBar.add(menu);

        menuBar.add(theme);
        JMenu align = new JMenu("Algin");
        align.setHorizontalTextPosition(SwingConstants.CENTER);
        align.setVerticalTextPosition(SwingConstants.BOTTOM);
        JMenuItem a = new JMenuItem("Align");
        a.setHorizontalTextPosition(SwingConstants.CENTER);
        a.setVerticalTextPosition(SwingConstants.BOTTOM);
        a.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                AlignmentPanel a = new AlignmentPanel(Viewer.this);
            }
        });
        align.add(a);
        menuBar.add(align);

        JMenu about = new JMenu("About");
        about.setHorizontalTextPosition(SwingConstants.CENTER);
        about.setVerticalTextPosition(SwingConstants.BOTTOM);
        JMenuItem ab = new JMenuItem("About");
        ab.setHorizontalTextPosition(SwingConstants.CENTER);
        ab.setVerticalTextPosition(SwingConstants.BOTTOM);
        ab.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                JDialog f = new AboutDialog(parent);
                f.show();
            }
        });
        about.add(ab);
        menuBar.add(about);
        return menuBar;
    }

    static class ApplicationCloser extends WindowAdapter
    {

        @Override
        public void windowClosing(WindowEvent e)
        {
            System.exit(0);
        }
    }

    static class JmolPanel extends JPanel
    {

        private static final long serialVersionUID = -3661941083797644242L;
        final Dimension currentSize = new Dimension();
        final Rectangle rectClip = new Rectangle();
        JmolSimpleViewer viewer;
        JmolAdapter adapter;

        JmolPanel()
        {
            adapter = new SmarterJmolAdapter();
            JmolPanel p = this;
            viewer = JmolSimpleViewer.allocateSimpleViewer(p, adapter);


        }

        public JmolSimpleViewer getViewer()
        {
            return viewer;
        }

        public void executeCmd(String rasmolScript)
        {
            viewer.evalString(rasmolScript);
        }

        @Override
        public void paint(Graphics g)
        {
            getSize(currentSize);
            g.getClipBounds(rectClip);
            try
            {
                viewer.renderScreenImage(g, currentSize.width,
                                         currentSize.height);
            }
            catch (Exception e)
            {
            }

        }
    }

    public class AboutDialog extends JDialog
    {

        public AboutDialog(JFrame parent)
        {
            super(parent, "About Alignment Viewer", true);
            setIconImage(parent.getIconImage());
            Box b = Box.createVerticalBox();

            Icon icon = UIManager.getIcon("OptionPane.questionIcon");
            JLabel label = new JLabel("", icon, JLabel.CENTER);
            b.add(Box.createGlue());
            getContentPane().add(b, "Center");
            JPanel p = new JPanel();
            p.add(label);
            getContentPane().add(p, BorderLayout.WEST);

            b = Box.createVerticalBox();
            b.add(Box.createGlue());
            JLabel lab = new JLabel(
                    "This is a small application " +
                    "for ROSS Alignment\r\n Algorithm.");
            lab.setFont(new Font(lab.getName(), Font.PLAIN, 12));

            b.add(lab);
            JLabel lab2 = new JLabel(
                    "The graphics are built on top off BioJava and JMOL Libries.");
            lab2.setFont(new Font(lab2.getName(), Font.PLAIN, 12));

            b.add(lab2);
            JLabel lab3 = new JLabel("Created by:\r\n FCI Team.");
            lab3.setFont(new Font(lab3.getName(), Font.PLAIN, 12));
            b.add(lab3);
            b.add(Box.createGlue());
            getContentPane().add(b, "Center");

            JPanel p2 = new JPanel();
            JButton ok = new JButton("Ok");
            p2.add(ok);
            getContentPane().add(p2, "South");

            ok.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent evt)
                {
                    setVisible(false);
                }
            });

            pack();
            setLocationRelativeTo(null);
            setResizable(false);
            setSize(400, 150);
        }
    }
}
