/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Viewer;

import Alignment.Alignment;
import Alignment.DescriptorComparor;

import Preprocessing.Preprocessing;
import Preprocessing.ProteinWriter;

import Viewer.AlignmentPanel.ProgressThreadDrawer;

import org.biojava.bio.structure.AminoAcidImpl;
import org.biojava.bio.structure.Atom;
import org.biojava.bio.structure.Calc;
import org.biojava.bio.structure.Chain;
import org.biojava.bio.structure.ChainImpl;
import org.biojava.bio.structure.Group;
import org.biojava.bio.structure.Structure;
import org.biojava.bio.structure.StructureException;
import org.biojava.bio.structure.StructureImpl;
import org.biojava.bio.structure.io.PDBFileReader;
import org.biojava.bio.structure.jama.Matrix;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jblas.DoubleMatrix;


/**
 *
 * @author Toshiba
 */
public class AlignmentPanel extends JFrame
{
    private final AlignmentPanel parent;
    private JButton              abrotButton;
    private JButton              startButton;
    private JFrame               infoWindow;
    private JProgressBar         progrss;
    private JTabbedPane          tab;
    private final JTextField     chain1Taba;
    private final JTextField     chain1Tabb;
    private final JTextField     chain2Taba;
    private final JTextField     chain2Tabb;
    private final JTextField     pdb1Taba;
    private final JTextField     pdb1Tabb;
    private final JTextField     pdb2Taba;
    private final JTextField     pdb2Tabb;
    private ProgressThreadDrawer drawer;
    private String               basePath;
    private final Viewer         viewer;

    public AlignmentPanel(Viewer viewer)
    {
        basePath = "D:\\Mine\\4th Year\\GP\\";
        this.pdb2Tabb = new JTextField();
        this.pdb2Taba = new JTextField();
        this.pdb1Tabb = new JTextField();
        this.chain2Tabb = new JTextField();
        this.chain2Taba = new JTextField();
        this.chain1Tabb = new JTextField();
        this.chain1Taba = new JTextField();
        this.pdb1Taba = new JTextField();

        setTitle("Alignment Panel");
        progrss = new JProgressBar();
        this.setIconImage(new ImageIcon(getClass().getResource("icon.png")).getImage());

        Dimension prefSize = progrss.getPreferredSize();
        prefSize.height = 25; //some width
        progrss.setPreferredSize(prefSize);
        progrss.setEnabled(false);
        tab = new JTabbedPane();
        tab.add("From ID", getIDPanel());
        tab.add("Custom Files", getCustomPanel());

        Container p = getContentPane();
        p.add(progrss, BorderLayout.PAGE_START);
        p.add(tab, BorderLayout.CENTER);
        startButton = new JButton("Start");
        tab.addChangeListener(new ChangeListener()
            {
                @Override
                public void stateChanged(ChangeEvent e)
                {
                }
            });

        startButton.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    int    index = tab.getSelectedIndex();
                    String p1 = (index == 0) ? pdb1Tabb.getText()
                                             : pdb1Taba.getText();
                    String p2 = (index == 0) ? pdb2Tabb.getText()
                                             : pdb2Taba.getText();
                    String c1 = (index == 0) ? chain1Tabb.getText()
                                             : chain1Taba.getText();
                    String c2 = (index == 0) ? chain2Tabb.getText()
                                             : chain2Taba.getText();

                    if (((p1 == null) || (p2 == null)) ||
                            (p1.isEmpty() || p2.isEmpty()))
                    {
                        JOptionPane.showMessageDialog(rootPane,
                                                      "You have" +
                                                      " to fill all " +
                                                      "required data!...",
                                                      "Error",
                                                      JOptionPane.ERROR_MESSAGE);
                    }
                    else
                    {
                        if (index == 1)
                        {
                            File pdp1 = new File(p1);
                            File pdp2 = new File(p1);

                            if (!pdp1.exists() || !pdp2.exists())
                            {
                                JOptionPane.showMessageDialog(rootPane,
                                                              "The entered " +
                                                              "PDB file is not" +
                                                              " exists!...",
                                                              "Error",
                                                              JOptionPane.ERROR_MESSAGE);
                            }
                        }

                        char chain1 = (c1.trim().length() > 0)
                                      ? c1.trim().charAt(0) : 'A';
                        char chain2 = (c2.trim().length() > 0)
                                      ? c2.trim().charAt(0) : 'A';

                        AlignemntThread thread = new AlignemntThread(index, p1,
                                                                     p2,
                                                                     chain1,
                                                                     chain2);
                        startUI(thread);
                        thread.start();
                    }
                }
            });

        abrotButton = new JButton("Abort");
        abrotButton.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    progrss.setIndeterminate(false);
                    startButton.setEnabled(true);
                    abrotButton.setEnabled(false);
                }
            });
        abrotButton.setEnabled(false);

        JPanel butom = new JPanel();
        butom.add(startButton, BorderLayout.PAGE_START);
        butom.add(abrotButton, BorderLayout.PAGE_START);
        p.add(butom, BorderLayout.PAGE_END);
        pack();
        setSize(470, 210);
        setLocationRelativeTo(null);
        setResizable(false);
        parent = this;
        setVisible(true);
        this.viewer = viewer;
    }

    private void drawAlignString(final DescriptorComparor comparor,
                                 final String title)
    {
        // final String qeuery =
        final String result = comparor.getInitalAlignmentString();

        class ViewPanel extends JPanel
        {
            private void draw(Graphics graphics)
            {
                Graphics2D g = (Graphics2D) graphics;
                this.setOpaque(true);
                this.setBackground(Color.white);
                g.setColor(Color.blue);

                Dimension size = getSize();
                Insets    insets = getInsets();

                int w = size.width - insets.left - insets.right;
                int h = size.height - insets.top - insets.bottom;

                String[] d = result.split("\r\n");
                int      x = 10;
                int      y = 20;
                int      startX = 392;
                int      startY = 0;
                int      secondX = x + 400;
                g.setFont(new Font("default", Font.BOLD, 14));

                if (comparor.getBestRotaion() == null)
                {
                    g.drawString(d[0], x, y);

                    return;
                }

                g.drawString("Initial Alginment.", x + 30, y);
                g.drawString("Final Alginment.", x + 485, y);
                y += 20;

                String[] pInfo = title.split("VS");
                g.setFont(new Font("default", Font.BOLD, 14));
                g.setColor(Color.black);
                g.drawString("Protein 1: " + pInfo[0] + ", " + d[0], x, y);
                g.drawString("Number of Aligned residues: " +
                             comparor.getNumOfAligned(), secondX, y);
                y += 20;
                g.drawString("Protein 2: " + pInfo[1] + ", " + d[1], x, y);
                g.drawString("Number of non Aligned residues: " +
                             comparor.getNumOfNonAligned(), secondX, y);
                y += 20;

                String[] out = d[9].split(",");
                g.drawString(out[0], x, y);
                g.drawString("Final LRMSD: " +
                             ProteinWriter.format(comparor.getLRMSED()),
                             secondX, y);
                y += 20;
                g.drawString(out[1], x, y);
                g.drawString("TM Score: " +
                             ProteinWriter.format(comparor.getTMScore()),
                             secondX, y);
                y += 20;
                g.drawString(out[2], x, y);
                g.drawString("Similarity : " +
                             ProteinWriter.format(comparor.getSimilarity()) +
                             "%", secondX, y);
                y += 20;

                int endX = startX;
                int endY = y + 20;
                g.setStroke(new BasicStroke(2, BasicStroke.CAP_ROUND,
                                            BasicStroke.JOIN_ROUND));
                g.drawLine(startX, startY, endX, endY - 10);
                g.drawString(d[10], x, y);
                g.drawString("Rotation Number: " +
                             comparor.getBestRotaionNumber(), secondX, y);
                g.drawLine(0, y + 10, w, y + 10);
                g.setColor(Color.black);
                g.drawRect(x + 5, y + 20, 20, 15);
                g.setColor(Color.blue);
                g.fillRect(x + 6, y + 21, 20 - 1, 15 - 1);
                g.drawString("Aligned", x + 40, y + 35);
                x += 130;
                g.setColor(Color.black);
                g.drawRect(x + 5, y + 20, 20, 15);
                g.setColor(Color.pink);
                g.fillRect(x + 6, y + 21, 20 - 1, 15 - 1);
                g.drawString("Not Aligned", x + 40, y + 35);
                x += 130;
                g.setColor(Color.black);
                g.drawRect(x + 5, y + 20, 20, 15);
                g.setColor(Color.red);
                g.fillRect(x + 6, y + 21, 20 - 1, 15 - 1);
                g.drawString("Miss", x + 40, y + 35);

                y += 60;
                x = 10;

                for (int i = 0; i < d[5].length(); i++)
                {
                    g.setColor(Color.black);
                    g.drawString(Character.toString(d[2].charAt(i)), x + 25, y);
                    g.drawString(Character.toString(d[3].charAt(i)), x + 25,
                                 y + 30);
                    g.drawString(Character.toString(d[4].charAt(i)), x + 25,
                                 y + 60);
                    g.drawString(Character.toString(d[6].charAt(i)), x + 25,
                                 y + 170);
                    g.drawString(Character.toString(d[7].charAt(i)), x + 25,
                                 y + 140);
                    g.drawString(Character.toString(d[8].charAt(i)), x + 25,
                                 y + 110);

                    if (d[5].charAt(i) == '|')
                    {
                        g.setColor(Color.black);
                        g.drawRect(x + 5, y + 70, 50, 15);
                        g.setColor(Color.blue);
                        g.fillRect(x + 7, y + 71, 50 - 2, 15 - 1);
                    }
                    else if (d[5].charAt(i) == '.')
                    {
                        g.setColor(Color.black);
                        g.drawRect(x + 5, y + 70, 50, 15);
                        g.setColor(Color.pink);
                        g.fillRect(x + 7, y + 71, 50 - 2, 15 - 1);
                    }
                    else
                    {
                        g.setColor(Color.black);
                        g.drawRect(x + 5, y + 70, 50, 15);
                        g.setColor(Color.red);
                        g.fillRect(x + 7, y + 71, 50 - 2, 15 - 1);
                    }

                    x += 50;

                    if (x >= (w - 55))
                    {
                        x = 10;
                        g.setFont(new Font("default", Font.BOLD, 18));

                        for (int j = 5; j <= (w - 5); j++)
                        {
                            g.setColor(Color.black);
                            g.drawString("_", j, y + 185);
                        }

                        y += 210;
                        g.setFont(new Font("default", Font.BOLD, 14));
                    }
                }
            }

            @Override
            public void paintComponent(Graphics g)
            {
                super.paintComponent(g);
                draw(g);
            }
        }
        infoWindow = new JFrame();
        infoWindow.add(new ViewPanel());
        infoWindow.setIconImage(this.getIconImage());
        infoWindow.pack();
        infoWindow.setTitle(title);
        infoWindow.setSize(800, 450);
        infoWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        infoWindow.setLocationRelativeTo(null);
        infoWindow.setVisible(true);
    }

    private Component getCustomPanel()
    {
        JPanel pane = new JPanel();
        pane.setLayout(null);

        JLabel  label = new JLabel("PDB 1:");
        JLabel  label3 = new JLabel("Chain 1:");
        JLabel  label2 = new JLabel("PDB 2:");
        JLabel  label4 = new JLabel("Chain 2:");
        JButton browse1 = new JButton("Browse");
        JButton browse2 = new JButton("Browse");

        pane.add(label);
        pane.add(pdb1Taba);
        pane.add(label3);
        pane.add(chain1Taba);
        pane.add(label2);
        pane.add(pdb2Taba);
        pane.add(label4);
        pane.add(chain2Taba);

        pane.add(browse1);
        pane.add(browse2);

        label.setLocation(10, 15);
        pdb1Taba.setLocation(45, 15);
        browse1.setLocation(220, 15);
        label3.setLocation(300, 15);
        chain1Taba.setLocation(340, 15);

        label.setSize(40, 25);
        pdb1Taba.setSize(170, 25);
        browse1.setSize(70, 25);
        label3.setSize(80, 25);
        chain1Taba.setSize(40, 25);

        label2.setLocation(10, 50);
        pdb2Taba.setLocation(45, 50);
        browse2.setLocation(220, 50);
        label4.setLocation(300, 50);
        chain2Taba.setLocation(340, 50);

        label2.setSize(40, 25);
        pdb2Taba.setSize(170, 25);
        browse2.setSize(70, 25);
        label4.setSize(80, 25);
        chain2Taba.setSize(40, 25);

        browse1.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    JFileChooser open = new JFileChooser();
                    int          c = open.showOpenDialog(parent);

                    if (c == JFileChooser.APPROVE_OPTION)
                    {
                        pdb1Taba.setText(open.getSelectedFile().toString());
                    }
                }
            });

        browse2.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    JFileChooser open = new JFileChooser();
                    int          c = open.showOpenDialog(parent);

                    if (c == JFileChooser.APPROVE_OPTION)
                    {
                        pdb2Taba.setText(open.getSelectedFile().toString());
                    }
                }
            });

        return pane;
    }

    private Component getIDPanel()
    {
        JPanel pane = new JPanel();
        pane.setLayout(null);

        JLabel label = new JLabel("PDB 1:");
        JLabel label3 = new JLabel("Chain 1:");
        JLabel label2 = new JLabel("PDB 2:");
        JLabel label4 = new JLabel("Chain 2:");

        pane.add(label);
        pane.add(pdb1Tabb);
        pane.add(label3);
        pane.add(chain1Tabb);
        pane.add(label2);
        pane.add(pdb2Tabb);
        pane.add(label4);
        pane.add(chain2Tabb);

        label.setLocation(40, 15);
        pdb1Tabb.setLocation(90, 15);
        label3.setLocation(200, 15);
        chain1Tabb.setLocation(250, 15);

        label.setSize(40, 25);
        pdb1Tabb.setSize(100, 25);
        label3.setSize(80, 25);
        chain1Tabb.setSize(100, 25);

        label2.setLocation(40, 50);
        pdb2Tabb.setLocation(90, 50);
        label4.setLocation(200, 50);
        chain2Tabb.setLocation(250, 50);

        label2.setSize(40, 25);
        pdb2Tabb.setSize(100, 25);
        label4.setSize(80, 25);
        chain2Tabb.setSize(100, 25);

        return pane;
    }

    public void setBasePath(String basePath)
    {
        this.basePath = basePath;
    }

    private void startUI(AlignemntThread th)
    {
        startButton.setEnabled(false);
        abrotButton.setEnabled(true);
        progrss.setEnabled(true);

        if (drawer != null)
        {
            drawer = new ProgressThreadDrawer(progrss, th);
            progrss.setIndeterminate(true);
            drawer.start();
        }
        else
        {
            progrss.setIndeterminate(false);

            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException ex)
            {
                Logger.getLogger(AlignmentPanel.class.getName())
                      .log(Level.SEVERE, null, ex);
            }

            drawer = new ProgressThreadDrawer(progrss, th);
            progrss.setIndeterminate(true);
            drawer.start();
        }
    }

    private void stopUI()
    {
        startButton.setEnabled(true);
        abrotButton.setEnabled(false);
        progrss.setEnabled(false);
        progrss.setIndeterminate(false);
    }

    class AlignemntThread extends Thread
    {
        String pdb1;
        String pdb2;
        char   chain1;
        char   chain2;
        int    index;

        public AlignemntThread(int i, String p1, String p2, char c1, char c2)
        {
            pdb1 = p1;
            pdb2 = p2;
            chain1 = c1;
            chain2 = c2;
            index = i;
        }

        private Chain getAtomsCA(Chain c1)
        {
            Chain c = new ChainImpl();

            for (Group g : c1.getAtomGroups())
            {
                if (g.hasAtom("CA"))
                {
                    Group gg = new AminoAcidImpl();

                    try
                    {
                        gg.addAtom(g.getAtom("CA"));
                        c.addGroup(gg);
                    }
                    catch (StructureException ex)
                    {
                        Logger.getLogger(AlignmentPanel.class.getName())
                              .log(Level.SEVERE, null, ex);
                    }
                }
            }

            return c;
        }

        @Override
        public void run()
        {
            PDBFileReader pdbr = new PDBFileReader();

            if (index == 1)
            {
                try
                {
                    Structure     struc1 = pdbr.getStructure(pdb1);
                    Structure     struc2 = pdbr.getStructure(pdb2);
                  
                    try
                    {
                        Preprocessing pre = new Preprocessing();
                        pre.preprocessAndWrite(pdb1);
                        pre = new Preprocessing();
                        pre.preprocessAndWrite(pdb2);
                        pdb1 = pdb1.substring(0, pdb1.lastIndexOf(".")) + ".txt";
                        pdb2 = pdb2.substring(0, pdb2.lastIndexOf(".")) + ".txt";

                        Alignment align = new Alignment(true);
                        align.align(pdb1, pdb2, chain1, chain2);

                        showAligned(struc1, struc2, align);
                        drawAlignString(align.getDescriptorComparor(),
                                        pdb1.substring(pdb1.lastIndexOf("\\") +
                                                       1, pdb1.lastIndexOf(".")) +
                                        " VS " +
                                        pdb2.substring(pdb2.lastIndexOf("\\") +
                                                       1, pdb2.lastIndexOf(".")));

                        try
                        {
                            File tmp = new File(pdb1);

                            if (tmp.exists())
                            {
                                tmp.delete();
                            }

                            tmp = new File(pdb2);

                            if (tmp.exists())
                            {
                                tmp.delete();
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    catch (Exception ex)
                    {
                        DescriptorComparor al = new DescriptorComparor();
                        if (pdb1 == null ? pdb2 == null : pdb1.equals(pdb2))
                        {
                            DoubleMatrix m = Model.Matrix.eye(4);
                            ArrayList<Model.Matrix> ar = new ArrayList<>();
                            Model.Matrix j = Model.Matrix.toParent(m);
                            ar.add(j);
                            ar.add(j);
                            al.setRotationMatrix(ar);
                            al.setInitialAlignmentString(new StringBuilder(
                                    "Same Protein"));
                            showAligned(struc1, struc2, null);
                        }
                        else
                        {
                            al.setInitialAlignmentString(new StringBuilder(ex.
                                    getMessage()));
                            showAligned(struc1, null, null);

                            al.setInitialAlignmentString(new StringBuilder(ex.
                                    getMessage()));
                        }
                        drawAlignString(al,
                                        pdb1.substring(pdb1.lastIndexOf("\\") +
                                                       1, pdb1.lastIndexOf(".")) +
                                        " VS " +
                                        pdb2.substring(pdb2.lastIndexOf("\\") +
                                                       1, pdb2.lastIndexOf(".")));
                    }
                }
                catch (IOException ex)
                {
                    Logger.getLogger(AlignmentPanel.class.getName())
                          .log(Level.SEVERE, null, ex);
                }
            }
            else
            {
                try
                {
                    Structure     struc1 = pdbr.getStructure(basePath +
                                                             "Files\\" + pdb1 +
                                                             ".pdb");
                    Structure     struc2 = pdbr.getStructure(basePath +
                                                             "Files\\" + pdb2 +
                                                             ".pdb");
                    pdb1 = basePath + "txt\\" + pdb1 + ".txt";

                    pdb2 = basePath + "txt\\" + pdb2 + ".txt";

                    Alignment align = new Alignment(true);

                    try
                    {
                        align.align(pdb1, pdb2, chain1, chain2);
                        showAligned(struc1, struc2, align);
                        drawAlignString(align.getDescriptorComparor(),
                                        pdb1.substring(pdb1.lastIndexOf("\\") +
                                                       1, pdb1.lastIndexOf(".")) +
                                        " VS " +
                                        pdb2.substring(pdb2.lastIndexOf("\\") +
                                                       1, pdb2.lastIndexOf(".")));
                    }
                    catch (Exception ex)
                    {
                        DescriptorComparor al = new DescriptorComparor();
                        if (pdb1 == null ? pdb2 == null : pdb1.equals(pdb2))
                        {
                            DoubleMatrix m = Model.Matrix.eye(4);
                            ArrayList<Model.Matrix> ar = new ArrayList<>();
                            Model.Matrix j = Model.Matrix.toParent(m);
                            ar.add(j);
                            al.setRotationMatrix(ar);
                            al.setInitialAlignmentString(new StringBuilder(
                                    "Same Protein"));
                            showAligned(struc1, struc2, null);
                        }
                        else
                        {
                            al.setInitialAlignmentString(new StringBuilder(ex.
                                    getMessage()));
                            showAligned(struc1, null, null);

                            al.setInitialAlignmentString(new StringBuilder(ex.
                                    getMessage()));
                        }
                        drawAlignString(al,
                                        pdb1.substring(pdb1.lastIndexOf("\\") +
                                                       1, pdb1.lastIndexOf(".")) +
                                        " VS " +
                                        pdb2.substring(pdb2.lastIndexOf("\\") +
                                                       1, pdb2.lastIndexOf(".")));
                    }
                }
                catch (IOException ex)
                {
                    Logger.getLogger(AlignmentPanel.class.getName())
                          .log(Level.SEVERE, null, ex);
                }
            }

            stopUI();
        }

        private void showAligned(Structure struc1, Structure struc2,
                                 Alignment align)
        {
            Chain c1 = null;
            Chain c2 = null;

            try
            {
                c1 = struc1.getChainByPDB(Character.toString(chain1));
            }
            catch (StructureException ex)
            {
                if (c1 == null)
                {
                    c1 = struc1.getChain(0);
                }
            }

            Structure outputStructure = new StructureImpl();
            outputStructure.setName(pdb1.contains(".") ?
                                    pdb1.substring(0, pdb1.indexOf(".")) : pdb1);

            if (struc2 == null || align == null)
            {
                if (struc2 == null)
                {
                    outputStructure.addChain(c1);
                    viewer.setStructure(outputStructure);
                    viewer.evulateString("select *:" + chain1 +
                                         ";color cartoon red");
                }
                else
                {
                    try
                    {
                        c2 = struc2.getChainByPDB(Character.toString(chain2));
                        c1.setChainID("A");
                        outputStructure.addChain(c1);
                        c2.setChainID("B");
                        outputStructure.addChain(c2);
                        viewer.setStructure(outputStructure);
                        viewer.evulateString("select *:" + c1.getChainID() +
                                             ";color cartoon yellow");
                        viewer.evulateString("select *:" + c2.getChainID() +
                                             ";color cartoon red");
                    }
                    catch (StructureException ex)
                    {
                        Logger.getLogger(AlignmentPanel.class.getName()).
                                log(Level.SEVERE, null, ex);
                    }
                }

                return;
            }

            try
            {
                c2 = struc2.getChainByPDB(Character.toString(chain2));
            }
            catch (StructureException ex)
            {
                if (c2 == null)
                {
                    c2 = struc1.getChain(0);
                }
            }

            int l = c1.getAtomLength();

            outputStructure.addChain(c2);

            Matrix m = new Matrix(align.getDescriptorComparor()
                                       .getRotationMatrix()
                                       .get(align.getDescriptorComparor()
                                                 .getRotationMatrix().size() -
                                            1).toArray2());
            Matrix shift = new Matrix(align.getDescriptorComparor()
                                           .getRotationMatrix()
                                           .get(align.getDescriptorComparor()
                                                     .getTranslationMatrix()
                                                     .size() - 1).toArray2());

            String tempC = "Z";
            String temp2 = "A";

            for (int i = 0; i < l; i++)
            {
                try
                {
                    Calc.rotate(c1.getAtomGroup(i), m);

                    Atom  a = (Atom) c1.getAtomGroup(i).getAtom("CA").clone();
                    Group g = c1.getAtomGroup(i);
                    a.setX(shift.get(0, 0));
                    a.setY(shift.get(0, 1));
                    a.setZ(shift.get(0, 2));
                    Calc.shift(g, a);
                    c1.setChainID(tempC);
                }
                catch (StructureException e)
                {
                }
            }

            outputStructure.addChain(c1);
            viewer.setStructure(outputStructure);
            viewer.evulateString("select *:" + temp2 + ";color cartoon red"); //lightblue");
            viewer.evulateString("select *:" + tempC + ";color cartoon yellow"); //aquamarine");
            viewer.evulateString("reset;");
        }
    }

    class ProgressThreadDrawer extends Thread
    {
        AlignemntThread      thread;
        private JProgressBar progress;
        private final int    interval = 100;

        public ProgressThreadDrawer(JProgressBar progress, AlignemntThread th)
        {
            this.progress = progress;
            this.thread = th;
        }

        @Override
        public void run()
        {
            boolean finished = false;

            synchronized (this)
            {
                while (!finished)
                {
                    try
                    {
                        progress.repaint();

                        if (!progress.isIndeterminate())
                        {
                            if ((thread != null) && thread.isAlive())
                            {
                                try
                                {
                                    thread.interrupt();
                                    thread.stop();
                                    stopUI();
                                }
                                catch (Exception e)
                                {
                                }
                                finally
                                {
                                    stopUI();
                                }
                            }

                            break;
                        }

                        wait(interval);
                    }
                    catch (InterruptedException e)
                    {
                    }

                    progress.repaint();
                }

                progress = null;
            }
        }
    }
}
