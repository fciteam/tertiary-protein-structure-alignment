package Viewer;

//~--- non-JDK imports --------------------------------------------------------
import java.awt.Frame;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.metal.DefaultMetalTheme;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.OceanTheme;

/**
 *
 * @author PC
 */
public class Controller
{

    private static Viewer window;

    public static String LOOKANDFEEL = "System";
    public static Object THEME = "Ocean";

    public Viewer getViewew()
    {
        return window;
    }

    /**
     *
     */
    public Controller()
    {
        window = null;
    }

    public static void initLookAndFeel()
    {
        String lookAndFeel;

        if (LOOKANDFEEL != null)
        {
            switch (LOOKANDFEEL)
            {
                case "Metal":
                    lookAndFeel = UIManager.
                            getCrossPlatformLookAndFeelClassName();
                    break;
                case "System":
                    lookAndFeel = UIManager.getSystemLookAndFeelClassName();
                    break;
                case "Motif":
                    lookAndFeel =
                    "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
                    break;
                case "GTK":
                    lookAndFeel = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
                    break;
                case "Nimbus":
                    lookAndFeel =
                    "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
                    break;
                default:
                    System.err.println(
                            "Unexpected value of LOOKANDFEEL specified: " +
                            LOOKANDFEEL);
                    lookAndFeel = UIManager.
                            getCrossPlatformLookAndFeelClassName();
                    break;
            }

            try
            {
                try
                {
                    UIManager.setLookAndFeel(lookAndFeel);
                    for (Frame frame : JFrame.getFrames())
                    {
                        updateLAFRecursively(frame);
                    }
                    if (window != null)
                    {
                        updateLAFRecursively(window);
                    }

                }
                catch (InstantiationException | IllegalAccessException ex)
                {
                    Logger.getLogger(Viewer.class.getName()).
                            log(Level.SEVERE, null, ex);
                }

                // If L&F = "metal", set the theme
                if (LOOKANDFEEL.equals("Metal"))
                {
                    if (THEME.equals("DefaultMetal"))
                    {
                        MetalLookAndFeel.
                                setCurrentTheme(new DefaultMetalTheme());
                    }
                    else if (THEME.equals("Ocean"))
                    {
                        MetalLookAndFeel.setCurrentTheme(new OceanTheme());
                    }
                    UIManager.setLookAndFeel(new MetalLookAndFeel());
                }

            }

            catch (ClassNotFoundException e)
            {
                System.err.println(
                        "Couldn't find class for specified look and feel:" +
                        lookAndFeel);
                System.err.println(
                        "Did you include the L&F library in the class path?");
                System.err.println("Using the default look and feel.");
            }

            catch (UnsupportedLookAndFeelException e)
            {
                System.err.println("Can't use the specified look and feel (" +
                                   lookAndFeel +
                                   ") on this platform.");
                System.err.println("Using the default look and feel.");
            }
        }
    }

    public static void updateLAFRecursively(java.awt.Window window)
    {
        for (java.awt.Window childWindow : window.getOwnedWindows())
        {
            updateLAFRecursively(childWindow);
        }
        if (window != null)
        {
            SwingUtilities.updateComponentTreeUI(window);
        }
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                JFrame.setDefaultLookAndFeelDecorated(true);
                initLookAndFeel();
                window = new Viewer();
            }
        });

    }
}
