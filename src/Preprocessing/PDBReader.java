package Preprocessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * @author Abdallah
 */
public class PDBReader implements Comparable<PDBReader>
{

    private String filename;
    private String ID;
    private ArrayList<String> helix;
    private ArrayList<String> sheet;
    private ArrayList<String> atom;
    private ArrayList<String> sequence;
    private ArrayList<Character> chains;

    /**
     * @param filename
     */
    public PDBReader(String filename)
    {
        super();
        this.filename = filename;
    }

    /**
     * @param filename
     * @param iD
     */
    public PDBReader(String filename, String iD)
    {
        super();
        this.filename = filename;
        ID = iD;
    }

    /**
     * This method used to get PDB file name.
     *
     * @return String.
     */
    public String getFilename()
    {
        return filename;
    }

    public ArrayList<String> getSequence()
    {
        return sequence;
    }

    public void setSequence(ArrayList<String> sequence)
    {
        this.sequence = sequence;
    }

    /**
     * This method used to set PDB file name.
     *
     * @param filename as {String}.
     */
    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    /**
     * This method used to get PDB file ID.
     *
     * @return String.
     */
    public String getID()
    {
        return ID;
    }

    /**
     * This method used to set PDB file ID.
     *
     * @param iD
     */
    public void setID(String iD)
    {
        ID = iD;
    }

    /**
     * This method used to get extracted helix from Protein file.
     *
     * @return {ArrayList<String>}.
     */
    public ArrayList<String> getHelix()
    {
        return helix;
    }

    /**
     * This method used to set data into helices vector.
     *
     * @param helix
     */
    public void setHelix(ArrayList<String> helix)
    {
        this.helix = helix;
    }

    public ArrayList<Character> getChains()
    {
        return chains;
    }

    public void setChains(ArrayList<Character> chains)
    {
        this.chains = chains;
    }

    /**
     * This method used to get extracted sheets from Protein file.
     *
     * @return {ArrayList<String>}.
     */
    public ArrayList<String> getSheet()
    {
        return sheet;
    }

    /**
     * This method used to set data into sheets vector.
     *
     * @param sheet
     */
    public void setSheet(ArrayList<String> sheet)
    {
        this.sheet = sheet;
    }

    @Override
    public int compareTo(PDBReader o)
    {
        return ID.compareTo(o.getID());
    }

    /**
     * This method used to read data from Protein file.
     *
     * @see Exception
     */
    public void readPDB() throws Exception
    {
        File file = null;

        if (filename == null || filename.isEmpty())
        {
            throw new NullPointerException("File name not set");
        }

        if (ID == null || ID.isEmpty())
        {
            file = new File(filename);
            ID = file.getName().substring(0, file.getName().indexOf('.'));
        }

        helix = new ArrayList<>();
        sheet = new ArrayList<>();
        atom = new ArrayList<>();
        sequence = new ArrayList<>();
        chains = new ArrayList<>();
        int st = 10000;
        int en = 0;
        char symbol = 0, c = 0;
        int i = 1;
        try (BufferedReader reader = new BufferedReader(new FileReader(file)))
        {
            String line;
            while ((line = reader.readLine()) != null)
            {
                String prevex = line.substring(0, 5).trim();
                if (prevex.equals("HELIX") || prevex.equals("helix"))
                {
                    helix.add(line);
                }
                else if (prevex.equals("SHEET") || prevex.equals("sheet"))
                {
                    sheet.add(line);
                }
                else if (prevex.equals("ATOM") || prevex.equals("atom"))
                {

                    c = line.charAt(21);
                    if (symbol == 0)
                    {
                        symbol = c;
                        st = i;

                    }
                    else
                    {
                        if (symbol == c)
                        {
                            if (st > i)
                            {
                                st = i;
                            }
                            else if (en < i)
                            {
                                en = i;
                            }
                        }
                        else
                        {
                            if (!chains.contains(symbol))
                            {
                                chains.add(symbol);
                                symbol = c;
                            }
                            else
                            {
                                symbol = c;
                            }
                        }
                    }

                    String l = line.substring(13, 16).trim();
                    if (l.equals("CA"))
                    {
                        atom.add(line);
                    }
                    i++;
                }
                else if (line.startsWith("TER") || line.startsWith("ter"))
                {
                    if (!chains.contains(symbol))
                    {
                        chains.add(symbol);
                    }
                    else
                    {
                        symbol = c;
                        st = i;
                        en = i;
                    }
                }
                else if (line.startsWith("SEQRES") || line.startsWith("seqres"))
                {
                    String s = line.substring(6);
                    sequence.add(s);
                }
                else if (line.startsWith("END") || line.startsWith("ENDMDL") ||
                         line.startsWith("end") || line.startsWith("endmdl"))
                {
                    break;
                }
            }
            if (!chains.contains(symbol))
            {
                chains.add(symbol);
            }
        }
        catch (Exception e)
        {
            throw new Exception("Wrong in reading protein file!");
        }
    }

    /**
     * This method used to get extracted atoms from Protein file.
     *
     * @return {ArrayList<String>}.
     */
    public ArrayList<String> getAtom()
    {
        return atom;
    }

    /**
     * This method used to set data into atoms vector.
     *
     * @param atom
     */
    public void setAtom(ArrayList<String> atom)
    {
        this.atom = atom;
    }
}
