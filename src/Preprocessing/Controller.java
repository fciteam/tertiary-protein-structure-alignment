package Preprocessing;

import Alignment.Alignment;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 * @author Abdallah
 */
public class Controller
{
    public static void alignmentForAll(String target)
    {
        long   now = System.currentTimeMillis();
        String path = "D:\\Mine\\4th Year\\GP\\txt\\";
        File   folder = new File(path);
        File[] files = folder.listFiles(new FilenameFilter()
            {
                @Override
                public boolean accept(File dir, String name)
                {
                    return name.endsWith(".txt") || name.endsWith(".TXT");
                }
            });

        Alignment alignment;
        int       n = 0;

        for (File file : files)
        {
            try
            {
                alignment = new Alignment();

                String query = file.getName()
                                   .substring(0, file.getName().lastIndexOf('.'));
                alignment.align(path + query + ".txt", target, 'A', 'A');
            }
            catch (Exception ex)
            {
                System.out.println(file.getName() + " " + ex.getMessage());
                n++;
            }
        }

        long t = System.currentTimeMillis() - now;
        t = t / (files.length - n);
        System.out.println("Passed " + (files.length - n) + " Failed " + n +
                           " " + t);
    }

    private void drawInitial(final String qeuery)
    {
        class ViewPanel extends JPanel
        {
            private void draw(Graphics g)
            {
                this.setOpaque(true);
                this.setBackground(Color.white);
                g.setColor(Color.blue);

                Dimension size = getSize();
                Insets    insets = getInsets();

                int w = size.width - insets.left - insets.right;
                int h = size.height - insets.top - insets.bottom;

                String[] d = qeuery.split("\r\n");
                int      x = 10;
                int      y = 20;

                for (int i = 0; i < d[2].length(); i++)
                {
                    g.setFont(new Font("default", Font.BOLD, 16));
                    g.setColor(Color.black);
                    g.drawString(Character.toString(d[2].charAt(i)), x + 25, y);
                    g.drawString(Character.toString(d[3].charAt(i)), x + 25,
                                 y + 30);
                    g.drawString(Character.toString(d[4].charAt(i)), x + 25,
                                 y + 60);
                    g.drawString(Character.toString(d[6].charAt(i)), x + 25,
                                 y + 170);
                    g.drawString(Character.toString(d[7].charAt(i)), x + 25,
                                 y + 140);
                    g.drawString(Character.toString(d[8].charAt(i)), x + 25,
                                 y + 110);

                    if (d[5].charAt(i) == '|')
                    {
                        g.setColor(Color.black);
                        g.drawRect(x + 5, y + 70, 50, 15);
                        g.setColor(Color.blue);
                        g.fillRect(x + 7, y + 71, 50 - 2, 15 - 1);
                    }
                    else
                    {
                        g.setColor(Color.black);
                        g.drawRect(x + 5, y + 70, 50, 15);
                        g.setColor(Color.red);
                        g.fillRect(x + 7, y + 71, 50 - 2, 15 - 1);
                    }

                    x += 50;

                    if (x >= (w - 55))
                    {
                        x = 10;
                        g.setFont(new Font("default", Font.BOLD, 18));

                        for (int j = 5; j <= (w - 5); j++)
                        {
                            g.setColor(Color.black);
                            g.drawString("_", j, y + 185);
                        }

                        y += 210;
                    }
                }
            }

            @Override
            public void paintComponent(Graphics g)
            {
                super.paintComponent(g);
                draw(g);
            }
        }

        JFrame f = new JFrame();
        f.add(new ViewPanel());
        f.pack();
        f.setSize(600, 250);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }

    /**
     * @param args
     * @throws java.lang.CloneNotSupportedException
     * @throws java.io.IOException
     */
    public static void main(String[] args)
                     throws CloneNotSupportedException, IOException, Exception
    {
        String path = "D:\\Mine\\4th Year\\GP\\Files\\";
        //Preprocessing p = new Preprocessing();
        //Alignment al = new Alignment();
        //al.align(path+"1H9C.txt", "1FSQ.txt");
        //p.preprocessAndWrite(path+"1FKA.pdb");

        //  processFirstTime(path);
        alignmentForAll("1FSQ.txt");
    }

    public static void processFirstTime(String path)
    {
        long now = System.currentTimeMillis();

        File folder = new File(path);
        File[] files = folder.listFiles(new FilenameFilter()
            {
                @Override
                public boolean accept(File dir, String name)
                {
                    return name.endsWith(".PDB") || name.endsWith(".pdb");
                }
            });

        int n = 0;

        for (File file : files)
        {
            Preprocessing query = new Preprocessing();

            try
            {
                query.preprocessAndWrite(path + file.getName());
            }
            catch (Exception ex)
            {
                System.out.println(file.getName() + " " + ex.getMessage());
                n++;
            }
        }

        long t = System.currentTimeMillis() - now;
        t = t / (files.length - n);
        System.out.println("Passed " + (files.length - n) + " Failed " + n +
                           " " + t);
    }
}
